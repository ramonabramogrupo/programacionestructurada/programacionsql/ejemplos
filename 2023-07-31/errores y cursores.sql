﻿DROP DATABASE IF EXISTS cursores ;
CREATE DATABASE cursores;
USE cursores;

/**
Creamos una tabla para recorrerla mediante los cursores
**/
CREATE TABLE numeros (
id int AUTO_INCREMENT,
valor int,
PRIMARY KEY(id)
);

INSERT INTO numeros (valor)
  VALUES (2),(4),(7),(20),(13);

SELECT * FROM numeros n;

/**
UN CURSOR NOS PERMITE EN MOVERMOS POR UNA TABLA LEYENDO REGISTRO A REGISTRO
**/
DROP PROCEDURE IF EXISTS cursorBasico;
CREATE PROCEDURE cursorBasico()
BEGIN
DECLARE variable1 int;
DECLARE variable2 int;

-- DECLARO CURSOR
DECLARE cursor1 CURSOR FOR 
  SELECT id,valor FROM numeros;

-- ABRO CURSOR
OPEN cursor1;

-- 1 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 2 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 3 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 4 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 5 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- este ultimo fetch produce una excepcion de tipo NOT FOUND
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- CERRRAR CURSOR
CLOSE cursor1;

END;

CALL cursorBasico();

/**
UN CURSOR NOS PERMITE EN MOVERMOS POR UNA TABLA LEYENDO REGISTRO A REGISTRO
CONTROLAR LA EXCEPCION DE TIPO NOT FOUND MOSTRANDO UN MENSAJE NO HAY MAS REGISTROS
**/
DROP PROCEDURE IF EXISTS cursorBasico;
CREATE PROCEDURE cursorBasico()
BEGIN
DECLARE variable1 int;
DECLARE variable2 int;

-- DECLARO CURSOR
DECLARE cursor1 CURSOR FOR 
  SELECT id,valor FROM numeros;

-- DECLARO MANIPULADOR DE EXCEPCIONES
DECLARE EXIT HANDLER FOR NOT FOUND
BEGIN
  SELECT 'NO HAY MAS';
END;

-- ABRO CURSOR
OPEN cursor1;

-- 1 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 2 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 3 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 4 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- 5 FETCH
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- este ultimo fetch produce una excepcion de tipo NOT FOUND
FETCH cursor1 INTO variable1,variable2;
SELECT variable1,variable2;

-- CERRRAR CURSOR
CLOSE cursor1;

END;

CALL cursorBasico();


/**
UN CURSOR NOS PERMITE  MOVERMOS POR UNA TABLA LEYENDO REGISTRO A REGISTRO
UTILIZAMOS UN WHILE
CONTROLAR LA EXCEPCION DE TIPO NOT FOUND PARA SALIR DEL WHILE
**/
DROP PROCEDURE IF EXISTS cursorBasico;
CREATE PROCEDURE cursorBasico()
BEGIN
DECLARE variable1 int;
DECLARE variable2 int;
DECLARE final boolean DEFAULT 0;

-- DECLARO CURSOR
DECLARE cursor1 CURSOR FOR 
  SELECT id,valor FROM numeros;

-- DECLARO MANIPULADOR DE EXCEPCIONES
DECLARE CONTINUE HANDLER FOR NOT FOUND
BEGIN
  SET final=1;
END;

-- ABRO CURSOR
OPEN cursor1;

-- 1 FETCH
FETCH cursor1 INTO variable1,variable2;
WHILE (final=0) DO 
  SELECT variable1,variable2;
  -- siguiente FETCH
  FETCH cursor1 INTO variable1,variable2;
END WHILE;

-- CERRRAR CURSOR
CLOSE cursor1;

END;

CALL cursorBasico();


-- procedimiento
-- Mostrar cada uno de los registros de la tabla numeros
-- utilizando un bucle

DROP PROCEDURE IF EXISTS p1;
CREATE PROCEDURE p1()
BEGIN
-- declarar variables
DECLARE registros int DEFAULT 0;
DECLARE contador int DEFAULT 0;

-- procesamiento
SELECT COUNT(*) INTO registros FROM numeros n;

# SET registros=(SELECT COUNT(*) FROM numeros n);
WHILE (contador<registros) DO 
  -- numeros[contador]
  SELECT * FROM numeros LIMIT contador,1;
  set contador=contador+1;
END WHILE;

END;

CALL p1();


-- quiero realizar el mismo ejercicio pero con cursores
-- antes de hacerlo quiero una serie de ejemplos de cursores

DROP PROCEDURE IF EXISTS cursor1 ;
CREATE PROCEDURE cursor1 ()
BEGIN
  -- declarar variables
  DECLARE valor int DEFAULT 0;
  DECLARE id int DEFAULT 0;
  
  -- declarar cursores
  DECLARE cursorPrimero CURSOR FOR
    SELECT 
      n.id,
      n.valor
    FROM numeros n;
  
  -- procesamiento
  
  --  abrimos el cursor
  OPEN cursorPrimero;
  
  -- utilizando el cursor
  FETCH cursorPrimero INTO id,valor;  -- leo el primer registro y guardo en las variables los campos
  
  -- cerramos el cursor
  CLOSE cursorPrimero;
  
  -- mostramos los valores de las variables
  SELECT id,valor;

END;

CALL cursor1();

-- el mismo ejemplo anterior pero sin cursores

DROP PROCEDURE IF EXISTS sinCursor1 ;
CREATE PROCEDURE sinCursor1 ()
BEGIN
  -- declarar variables
  DECLARE valor int DEFAULT 0;
  DECLARE id int DEFAULT 0;
  
  -- procesamiento
  
  SELECT n.id,n.valor INTO id,valor FROM numeros n LIMIT 0,1;
  
  -- mostramos los valores de las variables
  SELECT id,valor;

END;

call sinCursor1();


-- procedimiento
-- el mismo ejercicio pero sustituyendo el limit por fetch
DROP PROCEDURE IF EXISTS cursor2;
CREATE PROCEDURE cursor2()
BEGIN
-- declarar variables
  DECLARE contador int DEFAULT 0;
  DECLARE id int DEFAULT 0;
  DECLARE valor int DEFAULT 0;
  DECLARE registros int DEFAULT 0;
  
  -- declarar cursores
  DECLARE cursorPrimero CURSOR FOR
    SELECT n.id,n.valor FROM numeros n;
  
  -- procesamiento
  -- abrir cursor
  OPEN cursorPrimero;
  -- almaceno el numero total de registros en registros
  SELECT COUNT(*) INTO registros FROM numeros n;
  
  WHILE (contador<registros) DO 
    FETCH cursorPrimero INTO id,valor; -- almacenas en las variables los valores de los campos
    SELECT id,valor; -- muestro las variables
    SET contador=contador+1; -- incremento el contador
  END WHILE;
    
  -- cerrar cursor
  CLOSE cursorPrimero;

END;

call cursor2();

-- procedimiento
-- realiza lo mismo pero con excepciones

DROP PROCEDURE IF EXISTS cursor3;
CREATE PROCEDURE cursor3()
BEGIN
-- declarar variables
  DECLARE id int DEFAULT 0;
  DECLARE valor int DEFAULT 0;
  DECLARE ultimo boolean DEFAULT FALSE;
  
  -- declarar cursores
  DECLARE cursorPrimero CURSOR FOR
    SELECT n.id,n.valor FROM numeros n;

  -- manipulador de excepciones
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET ultimo=1;
  # DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' set ultimo=1;
  # DECLARE CONTINUE HANDLER FOR 1329 set ultimo=1;
  
  -- procesamiento
  -- abrir cursor
  OPEN cursorPrimero;

  FETCH cursorPrimero INTO id,valor; -- almacenas en las variables los valores de los campos
  WHILE (ultimo=0) DO 
    SELECT id,valor; -- muestro las variables
    FETCH cursorPrimero INTO id,valor; -- almacenas en las variables los valores de los campos
  END WHILE;
    
  -- cerrar cursor
  CLOSE cursorPrimero;

END;

CALL cursor3();


-- procedimiento pares
-- procedimiento que busca los numeros pares de la tabla y los muestra concatenados con un "-"
-- utilizando cursores y excepciones (NOT FOUND)
-- call pares() ==> 2-4-20

DROP PROCEDURE IF EXISTS pares;
CREATE PROCEDURE pares()
BEGIN
-- declarar variables
DECLARE salida varchar(100) DEFAULT NULL;
DECLARE numero int DEFAULT 0;
DECLARE ultimo boolean DEFAULT FALSE;

-- declarar cursores
DECLARE cursor1 CURSOR FOR
  SELECT n.valor FROM numeros n;
-- manipulador de excepciones
declare CONTINUE HANDLER FOR NOT FOUND SET ultimo=1;


-- procesamiento
-- abrir cursor
OPEN cursor1;

FETCH cursor1 INTO numero;
WHILE (ultimo=0) DO 
  IF (MOD(numero,2)=0) THEN
    set salida=CONCAT_WS("-",salida,numero);   
  END IF;
  
  FETCH cursor1 INTO numero;
END WHILE;

-- cerrar cursor
CLOSE cursor1;

SELECT salida;

END;

CALL pares();