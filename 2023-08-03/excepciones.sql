DROP DATABASE IF EXISTS excepciones;
CREATE DATABASE excepciones;
USE excepciones;

/** voy a crear una tabla de errores donde almacenare cuando se produzca un error **/

CREATE TABLE errores(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  fecha datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
);


/**
crear un procedimiento que intente leer una tabla que no exista
que controle el error
**/

-- Utilizando SQLEXCEPTION 

DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores

-- procesamiento
SELECT * FROM noexiste;
END;

CALL uno();

-- utilizando Codigo error
-- 1146
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR 1146
BEGIN
  SELECT 'error';
END;

-- procesamiento
SELECT * FROM noexiste;
END;

CALL uno();


-- utilizando SQLSTATE
-- 42S02
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR SQLSTATE '42S02'
BEGIN
  SELECT 'error';
END;

-- procesamiento
SELECT * FROM noexiste;
END;

CALL uno();


-- UTILIZAMOS CONDICIONES CON SQLSTATE
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
DECLARE noExiste CONDITION FOR SQLSTATE '42S02';

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR noExiste
BEGIN
  SELECT 'error';
END;

-- procesamiento
SELECT * FROM noexiste;
END;

CALL uno();


-- crear una excepcion nosotros 
-- utilizando signal
-- de tipo '42s02'
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
DECLARE noExiste CONDITION FOR SQLSTATE '42S02';

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR noExiste
BEGIN
  SELECT 'error';
END;

-- procesamiento
SIGNAL noExiste;

END;

CALL uno();

-- crear una excepcion nosotros 
-- utilizando signal
-- de tipo '42s02'
-- MOSTRANDO UN ERROR 'ERROR GRAVE'
-- PARA ELLO NO CONTROLEIS LA EXCEPCION
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
BEGIN
-- declarar variables
-- declarar condiciones
DECLARE noExiste CONDITION FOR SQLSTATE '42S02';

-- declarar manipuladores
-- procesamiento
SIGNAL noExiste set MESSAGE_TEXT='ERROR GRAVE';

END;

CALL uno();


-- Realizamos algunos ejemplos 

/**
Crear un procedimiento que controle todas las excepciones
de tipo SQLEXCEPTION
Nos debe a�adir un registro a la tabla errores
**/

DROP PROCEDURE IF EXISTS errores1;
CREATE PROCEDURE errores1()
BEGIN
-- declarar variables
-- declarar condiciones

-- declarar manipuladores
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN
  INSERT INTO errores (nombre) VALUES ('error 1');
END;

-- procesamiento
-- al intentar listar una tabla no existente produce una excepcion de tipo EXCEPTION
SELECT * FROM tonteria;
-- esta instruccion no se va a ejecutar porque el manipulador es de tipo EXIT
SELECT * FROM errores;
END;

CALL errores1();

SELECT * FROM errores e;

/**
CREAR UN PROCEDIMIENTO ALMACENADO QUE CONTROLE LAS EXCEPCIONES DE 
ERROR 1051(CUANDO INTENTO ELIMINAR UNA TABLA QUE NO EXISTE)

A�ADIR UN REGISTRO A LA TABLA ERRORES
**/

DROP PROCEDURE IF EXISTS errores2;
CREATE PROCEDURE errores2()
BEGIN
-- declarar variables
DECLARE CONTINUE HANDLER FOR 1051 
BEGIN 
  INSERT INTO errores (nombre) VALUES ('error en el procedimiento errores 2');
END;
-- procesamiento
-- al intentar borrar una tabla que no existe lanza la excepcion
DROP TABLE tonteria;
-- esta instruccion se ejecuta porque el manipulador es de tipo continue
SELECT * FROM errores;
END;

CALL errores2();

-- conocer el codigo de error que se produce

SELECT * FROM tonterias;
SHOW ERRORS; -- error 1146

DROP TABLE tonterias;
SHOW ERRORS; -- error 1051

CREATE TABLE errores(
id int
);
SHOW ERRORS; -- error 1050

/**
CREAR UN PROCEDIMIENTO ALMACENADO QUE CONTROLE LAS EXCEPCIONES DE 
ERROR 1051(CUANDO INTENTO ELIMINAR UNA TABLA QUE NO EXISTE)
A�ADIR UNA CONDICION DE ERROR DENOMINADA TABLANOEXISTE
A�ADIR UN REGISTRO A LA TABLA ERRORES
**/


DROP PROCEDURE IF EXISTS errores3;
CREATE PROCEDURE errores3()
BEGIN
-- declarar variables
-- declarar condiciones
DECLARE tablaNoExiste CONDITION FOR 1051;

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR tablaNoExiste
BEGIN 
INSERT INTO errores (nombre) VALUES ('error en el procedimiento errores 3');
END;

-- procesamiento
DROP TABLE tonterias;

SELECT * FROM errores;
END;

CALL errores3();

/**
  CREAMOS UNA TABLA NUMEROS
**/

DROP TABLE IF EXISTS numeros;
CREATE TABLE numeros(
  id int,
  valor int NOT NULL,
  PRIMARY KEY(id),
  UNIQUE KEY(valor)
);

-- error cuando dejo el campo requerido con NULL
INSERT INTO numeros (id,valor)  VALUES (1,NULL);
SHOW ERRORS; -- error 1048

-- error cuando no coloco nada en un campo requerido
INSERT INTO numeros (id) VALUES (1);
SHOW ERRORS; -- error 1364

/**
CREAR UN PROCEDIMIENTO QUE CONTROLE
LAS EXCEPCIONES 1364 Y 1048
(1364)error cuando no coloco nada en un campo requerido
(1048)error cuando dejo el campo requerido con NULL
A�ADIR UN REGISTRO A LA TABLA ERRORES
**/

DROP PROCEDURE IF EXISTS errores4;
CREATE PROCEDURE errores4()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR 1364,1048
BEGIN
  INSERT INTO errores (nombre) VALUES ('no puedo introducir registros en la tabla numeros sin los campos requeridos');
END;
-- procesamiento
INSERT INTO numeros (id) VALUES (1); -- lanza excepcion 1364 porque falta valor que es requerido
INSERT INTO numeros(id,valor) VALUES (1,NULL); -- lanza excepcion 1048 porque un campo es requerido y le paso valor NULL

END;

call errores4();
SELECT * FROM numeros;
SELECT * FROM errores;


INSERT INTO numeros (id, valor) VALUES (1, 1);

-- error cuando el dato introducido salta la restriccion de unique key o primary key
INSERT INTO numeros (id, valor) VALUES (1, 2);
SHOW ERRORS; -- error 1062

INSERT INTO numeros (id, valor) VALUES (2, 1);
SHOW ERRORS; -- error 1062

/**
CREAR UN PROCEDIMIENTO ALMACENADO
QUE CONTROLE EL ERROR DE INTRODUCIR DATOS REPETIDOS
A�ADIR UN REGISTRO A LA TABLA ERRORES
**/

DROP PROCEDURE IF EXISTS errores5;
CREATE PROCEDURE errores5()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR 1062 
BEGIN
INSERT INTO errores (nombre) VALUES ('no podemos meter datos repetidos en la tabla numeros');
END;
-- procesamiento
INSERT INTO numeros (id, valor) VALUES (2, 1); -- lanza la excepcion 1062 porque hay datos repetidos

END;

CALL errores5();
SELECT * FROM errores e;


/**
CREAR UN PROCEDIMIENTO QUE CONTROLE
LAS EXCEPCIONES 1364 Y 1048
(1364)error cuando no coloco nada en un campo requerido
(1048)error cuando dejo el campo requerido con NULL
A�ADIR UN REGISTRO A LA TABLA ERRORES CON MENSAJE DIFERENTE EN CADA ERROR
**/

DROP PROCEDURE IF EXISTS errores6;
CREATE PROCEDURE errores6()
BEGIN
-- declarar variables
-- declarar condiciones
-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR 1364,1048
BEGIN
  INSERT INTO errores (nombre) VALUES ('no puedo introducir registros en la tabla numeros sin los campos requeridos');
END;

DECLARE CONTINUE HANDLER FOR 1062 
BEGIN
INSERT INTO errores (nombre) VALUES ('no podemos meter datos repetidos en la tabla numeros');
END;

-- procesamiento
INSERT INTO numeros (id, valor) VALUES (2, 1); -- lanza la excepcion 1062 porque hay datos repetidos
INSERT INTO numeros (id) VALUES (1); -- lanza excepcion 1364 porque falta valor que es requerido
END;

CALL errores6();
SELECT * FROM errores e;

/**
CREAR UN PROCEDIMIENTO QUE CONTROLE
LAS EXCEPCIONES 1364 Y 1048
A�ADIR DOS CONDICIONES PARA CADA EXCEPCION
(1364)error cuando no coloco nada en un campo requerido
(1048)error cuando dejo el campo requerido con NULL
A�ADIR UN REGISTRO A LA TABLA ERRORES
**/

DROP PROCEDURE IF EXISTS errores7;
CREATE PROCEDURE errores7()
BEGIN
-- declarar variables
-- declarar condiciones
DECLARE requeridoNULL CONDITION FOR 1048;
DECLARE  requerido CONDITION FOR 1364;

DECLARE duplicado CONDITION FOR 1062;

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR requerido,requeridoNULL
BEGIN
  INSERT INTO errores (nombre) VALUES ('no puedo introducir registros en la tabla numeros sin los campos requeridos');
END;

DECLARE CONTINUE HANDLER FOR duplicado 
BEGIN
INSERT INTO errores (nombre) VALUES ('no podemos meter datos repetidos en la tabla numeros');
END;

-- procesamiento
INSERT INTO numeros (id, valor) VALUES (2, 1); -- lanza la excepcion 1062 porque hay datos repetidos
INSERT INTO numeros (id) VALUES (1); -- lanza excepcion 1364 porque falta valor que es requerido
END;

CALL errores7();

SELECT * FROM errores;



/**
Funcion que retorna par o impar.
Le pasas un numero y debe indicar si es par o impar
**/


DROP FUNCTION IF EXISTS errores1;
CREATE FUNCTION errores1(numero int)
RETURNS varchar(20) DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida varchar(20) DEFAULT 'impar';
-- procesamiento
IF (MOD(numero,2)=0) THEN
  set salida='par';
END IF;

-- devolver resultado
RETURN salida;
END;

SELECT errores1(2);

/**
REALIZAR UNA FUNCION QUE DEVUELVE



DROP FUNCTION IF EXISTS errores1;
CREATE FUNCTION errores1(numero int)
RETURNS varchar(20) DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida varchar(20) DEFAULT NULL;

-- declarar condiciones
DECLARE par CONDITION FOR SQLSTATE '45000';
DECLARE impar CONDITION FOR SQLSTATE '45001';

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR par 
BEGIN
  set salida="par"; 
END;

DECLARE CONTINUE HANDLER FOR impar 
BEGIN 
  set salida="impar";
END;

-- procesamiento
IF (MOD(numero,2)=0) THEN
  SIGNAL par;
ELSE
  SIGNAL impar;
END IF;

-- devolver resultado
RETURN salida;
END;

SELECT errores1(2);


/** 
funcion que le pasas una fecha y debe indicarme si el laboral o festivo
de lunes a viernes laboral y sabado o domingo festivo
Utilizando excepciones y SIGNAL
Consejo: funcion DAYOFWEEK(fecha)
**/

DROP FUNCTION IF EXISTS errores2;
CREATE FUNCTION errores2(fecha datetime)
RETURNS varchar(20)   DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida varchar(20) DEFAULT NULL;
-- declarar condiciones
DECLARE laboral CONDITION FOR SQLSTATE '45000';
DECLARE festivo CONDITION FOR SQLSTATE '45001';

-- declarar manipuladores
DECLARE CONTINUE HANDLER FOR laboral
BEGIN 
  SET salida="laboral";
END;

DECLARE CONTINUE HANDLER FOR festivo
BEGIN 
  SET salida="festivo";
END;
-- procesamiento
IF (DAYOFWEEK(fecha) IN (1,7)) THEN
  SIGNAL festivo;
ELSE 
  SIGNAL laboral;
END IF;

-- devolver resultado
RETURN salida;
END;

SELECT errores2('2023-08-07');