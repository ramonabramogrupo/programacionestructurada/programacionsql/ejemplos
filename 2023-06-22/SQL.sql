﻿USE ciclistas;

/** funcion 1

-- crear una funcion que recibe un texto y me indicar el numero de caracteres del
-- texto

**/

DROP FUNCTION IF EXISTS funcion1;
CREATE FUNCTION funcion1(texto varchar(100))
RETURNS int DETERMINISTIC
BEGIN
-- crear las variables
DECLARE resultado int DEFAULT 0;
-- procesamiento
SET resultado=CHAR_LENGTH(texto);
-- SELECT CHAR_LENGTH(texto) INTO resultado; -- realizo lo mismo que set
-- salida
RETURN resultado;
END;

SELECT funcion1("hola clase");

SELECT funcion1(c.nombre),c.nombre from ciclista c;

/**
Crear procedimiento  que genere dos tablas denominadas cliente y categorias
**/

DROP PROCEDURE IF EXISTS crearCliente;
CREATE PROCEDURE crearCliente()
BEGIN
-- declarar variables

-- procesamiento
DROP TABLE IF EXISTS cliente;
CREATE TABLE cliente(
  id int,
  nombre varchar(100),
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS categorias;
CREATE TABLE categorias(
  id int,
  nombre varchar(100),
  PRIMARY KEY(id)
); 

END;


-- ejecuto el procedimiento
CALL crearCliente();

/**

Crear un procedimiento que me genere la relacion entre las dos tablas
Necesito crear una tabla denomina TIENE que me relacione las tablas
cliente y categorias con cardinalidad n-n

**/

DROP PROCEDURE IF EXISTS crearRelacion;
CREATE PROCEDURE crearRelacion()
BEGIN
-- declarar variables

-- procesamiento
DROP TABLE IF EXISTS tiene;
CREATE TABLE tiene(
  idCliente int,
  idCategoria int,
  PRIMARY KEY(idCliente,idCategoria),
  CONSTRAINT fkTieneCliente FOREIGN KEY (idCliente) REFERENCES cliente(id),
  CONSTRAINT fkTieneCategorias FOREIGN KEY (idCategoria) REFERENCES categorias(id) 
);

END;

-- llamo al procedimiento
CALL crearRelacion();

/**
crear un procedimiento que elimine las claves ajenas

**/

DROP PROCEDURE IF EXISTS eliminarAjenas;
CREATE PROCEDURE eliminarAjenas()
BEGIN
-- declarar variables

-- procesamiento
ALTER TABLE tiene DROP CONSTRAINT fkTieneCliente, DROP CONSTRAINT fkTieneCategorias;

END;

-- llamamos al procedimiento
call eliminarAjenas();


/** crear procedimiento 
para restaurar la bbdd entera 

utilizando los procedimientos creados
***/

DROP PROCEDURE IF EXISTS restaurar;
CREATE PROCEDURE restaurar()
BEGIN
-- declarar variables

-- procesamiento
CALL eliminarAjenas();
CALL crearCliente();
CALL crearRelacion();

END;

-- llamar al procedimiento

CALL restaurar();


/*** 
procedimiento con 3 argumentos y que los muestre unidos
por comas
**/

DROP PROCEDURE IF EXISTS procedimiento1;
CREATE PROCEDURE procedimiento1(dato1 varchar(10),dato2 varchar(10), dato3 varchar(10))
BEGIN
-- declarar variables
DECLARE resultado varchar(50);
-- procesamiento
set resultado=CONCAT_WS(",",dato1,dato2,dato3);

SELECT resultado;

END;

CALL procedimiento1("ramon","abramo","feijoo");



