﻿-- creo el entorno para realizar los disparadores
DROP DATABASE IF EXISTS disparadores;
CREATE DATABASE disparadores;
USE disparadores;

CREATE TABLE noticias(
id int AUTO_INCREMENT,
texto varchar(100),
titulo varchar(100),
longitud int,
fecha date,
dia int,
mes varchar(20),
PRIMARY KEY(ID)
);

INSERT INTO noticias (texto, titulo,fecha) VALUES 
  ('ejemplo de noticia larga', 'larga', '2022/12/1'),
  ('ejemplo de noticia corta', 'corta', '2022/1/12');


CREATE TABLE historico(
  id int AUTO_INCREMENT,
  dia int,
  numero int DEFAULT 0,
  PRIMARY KEY (id)
);



# DISPARADOR
# CALCULA LA LONGITUD DE CARACTERES DEL
# TEXTO DE UNA NOTICIA 
# CUANDO CREO UNA NOTICIA NUEVA

DROP TRIGGER IF EXISTS noticiasBI;
CREATE TRIGGER noticiasBI
BEFORE INSERT
  ON  noticias
  FOR EACH ROW
BEGIN
  SET NEW.longitud=CHAR_LENGTH(NEW.texto);
END ;


SELECT * FROM NOTICIAS;

INSERT INTO noticias (texto, titulo, fecha) VALUES 
  ('esta noticia es nueva', 'nueva', '2022/12/12');


# DISPARADOR
# Realizar un disparador para que cuando 
# creemos una noticia nueva nos calcule el mes 
# y el día de la fecha y los introduzca 
# en los campos mes y día

DROP TRIGGER IF EXISTS noticiasBI2;
CREATE TRIGGER noticiasBI2
BEFORE INSERT
  ON noticias
  FOR EACH ROW
BEGIN
  SET LC_TIME_NAMES='es_es';
  SET NEW.dia=DAY(NEW.fecha), 
      NEW.mes=MONTHNAME(NEW.fecha);
END ;


INSERT INTO noticias (texto, titulo, fecha) VALUES 
  ('esta noticia es mas nueva', 'mas nueva', '2022/1/12');

SELECT * FROM noticias n;

# VOY A ACTUALIZAR UNA DE LAS NOTICIAS
UPDATE noticias n
  SET n.texto="algo"
  WHERE n.id=4;

SELECT * FROM noticias n;

# Realizar un disparador para que cuando ACTUALICEMOS 
# una noticia nos calcule el mes y el día de la fecha 
# y los introduzca en los campos mes y día 
# y la longitud del texto y lo coloque en el campo longitud
# Como nombre NoticiasBU

DROP TRIGGER IF EXISTS noticiasBU;
CREATE TRIGGER noticiasBU
BEFORE UPDATE
  ON noticias
  FOR EACH ROW
BEGIN
  SET LC_TIME_NAMES='es_es';
  SET NEW.dia=DAY(NEW.fecha), 
      NEW.mes=MONTHNAME(NEW.fecha), 
      NEW.longitud=CHAR_LENGTH(NEW.texto);

END ;

# VOY A ACTUALIZAR UNA DE LAS NOTICIAS
UPDATE noticias n
  SET n.texto="ejemplo", n.fecha='2023-10-15'
  WHERE n.id=1;

SELECT * FROM noticias n;

# tabla de historico


# Necesito una instrucción de SQL que me INICIALICE 
# la tabla HISTORICO con numero
# de noticias de ese dia


-- consulta de seleccion
SELECT COUNT(*) numero, DAY(n.fecha) 
FROM noticias n 
GROUP BY DAY(n.fecha);

-- consulta de creacion

DROP TABLE IF EXISTS historico;

CREATE TABLE historico(
  id int AUTO_INCREMENT,
  dia int,
  numero int DEFAULT 0,
  PRIMARY KEY (id)
) AS 
   SELECT COUNT(*) numero, dia
    FROM noticias n 
    GROUP BY dia;

SELECT * FROM historico h;


# disparador de eliminacion
# cada vez que ELIMINO una noticia de la tabla NOTICIAS
# me ACTUALICE la tabla HISTORICO 

DROP TRIGGER IF EXISTS noticiasAD;
CREATE TRIGGER noticiasAD
AFTER DELETE
  ON noticias
  FOR EACH ROW
BEGIN
  UPDATE historico h
    SET numero=numero-1
    WHERE 
      dia=OLD.dia;
END ;


# inserto registros nuevos en noticias
INSERT INTO noticias (texto, titulo, fecha)
  VALUES ('ejemplo noticias', 'ti1', '2022/5/8'),('ejemplo noticias 1', 'ti2', '2022/8/7');

# disparador
# Crear un DISPARADOR que me ACTUALICE la tabla HISTORICO 
# cada vez que INSERTO una noticia en la tabla NOTICIAS

DROP TRIGGER IF EXISTS noticiasAI;
CREATE TRIGGER noticiasAI
AFTER INSERT
  ON noticias
  FOR EACH ROW
BEGIN
    DECLARE dia int DEFAULT 0;
    DECLARE contador int DEFAULT 0;

    set dia=NEW.dia; -- esta variable almacena el dia calculado con el disparador anterior
   
    -- cuantos registros del mismo dia que la noticia
    -- introducida tengo en historico
    -- si es 1 ese dia ya esta en historico
    -- es es 0 ese dia no esta en historico
    -- opcion 1
    SELECT COUNT(*) INTO contador FROM historico h WHERE h.dia=dia;
    -- opcion 2
    -- SET contador=(SELECT COUNT(*) INTO contador FROM historico h WHERE h.dia=dia);
    
    IF (contador=1) THEN
      -- si contador es 1 indica que en historico ya tenia noticias de ese dia
      -- solamente tengo que sumar 1 a numero
      UPDATE historico h
        SET h.numero=h.numero+1
        WHERE h.dia=dia;
    ELSE 
      -- si contador es 0 indica que no tenia noticias de ese dia 
      -- tengo que insertar un registro nuevo en historico
      INSERT INTO historico (dia, numero)
        VALUES (dia,1);
    END IF;

END ;


SELECT * FROM noticias;
SELECT * FROM historico h;

INSERT INTO noticias (texto, titulo, fecha)
  VALUES ('EJEMPLO NUEVA', 'nueva','2022/5/19'),('EJEMPLO NUEVA 1', 'nueva 2','2022/5/8');


# quiero que si actualizo la noticia me actualice la tabla historico

DROP TRIGGER IF EXISTS noticiasAU;
CREATE TRIGGER noticiasAU
AFTER UPDATE
  ON  noticias
  FOR EACH ROW
BEGIN

  -- comprobar si tengo que realizar algo
  IF (NEW.dia<>OLD.dia) THEN
    UPDATE historico h
      SET h.numero=h.numero-1
      WHERE h.dia=OLd.dia;
    IF (SELECT COUNT(*) FROM historico h WHERE h.dia=NEW.dia) THEN
      UPDATE historico h
      SET h.numero=h.numero+1
      WHERE h.dia=NEW.dia;
    ELSE 
      INSERT INTO historico (dia, numero) VALUES (NEW.dia, 1);
    END IF;
  END IF;


  
END ;
