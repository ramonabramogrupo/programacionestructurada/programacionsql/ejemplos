﻿DROP DATABASE IF EXISTS ejemplosprogramacion;
CREATE DATABASE ejemploprogramacion;
USE ejemploprogramacion;

/**
  bucles
**/

-- funcion f1
-- Función que recibe como argumento un numero 
-- y me devuelve la suma de todos los números enteros desde 1 hasta el numero pasado
-- Numero pasado siempre mayor que 1

-- f1(45); -> 1+2+3+4+5+.....+45

DROP FUNCTION IF EXISTS f1;
CREATE FUNCTION f1(dato int)
RETURNS int DETERMINISTIC
BEGIN
-- declarar variables
DECLARE resultado int DEFAULT 0;
DECLARE contador int DEFAULT 1;

-- procesamiento
WHILE (contador<=dato) DO 
  set resultado=resultado+contador;
  set contador=contador+1;
END WHILE;

-- devolver resultado
RETURN resultado;
END;

SELECT f1(5);


-- funcion f1
-- Función que recibe como argumento un numero 
-- y me devuelve la suma de todos los números enteros desde 1 hasta el numero pasado
-- Numero pasado siempre mayor que 1
-- utilizamos un algoritmo diferente 
-- numero*(numero+1)/2

DROP FUNCTION IF EXISTS f1;
CREATE FUNCTION f1(dato int)
RETURNS int DETERMINISTIC
BEGIN
-- declarar variables
DECLARE resultado int DEFAULT 0;
-- procesamiento
set resultado=dato*(dato+1)/2;

-- devolver resultado
RETURN resultado;
END;


SELECT f1(15);

-- FUNCION f2
-- REALIZAR UNA FUNCION QUE ME DEVUELVA LA SUMA DE LOS
-- NUMEROS PARES ENTRE 1 Y EL NUMERO QUE LE PASEMOS COMO ARGUMENTO
-- EL NUMERO PASADO SIEMPRE SERA MAYOR QUE 1
-- f2(9) -> 2+4+6+8

DROP FUNCTION IF EXISTS f2;
CREATE FUNCTION f2(dato int)
RETURNS int DETERMINISTIC
BEGIN
-- declarar variables
DECLARE resultado int DEFAULT 0;
DECLARE contador int DEFAULT 2;
-- procesamiento
WHILE (contador<=dato) DO 
  set resultado=resultado+contador;
  SET contador=contador+2;
END WHILE;
-- devolver resultado
RETURN resultado;
END;

SELECT f2(9);

-- procedimiento p1
-- procedimiento que recibe como argumento un numero 
-- y me crea una tabla llamada numeros(id int primary key) con los numeros desde 1 hasta el valor pasado como argumento
-- Numero pasado siempre mayor que 1
-- call p1(5)
-- id(PK)
-- 1
-- 2
-- 3
-- 4
-- 5

DROP PROCEDURE IF EXISTS p1;

CREATE PROCEDURE p1(dato int)
BEGIN
  -- declarar variables
  DECLARE contador int DEFAULT 1;
  
  -- procesamiento
  DROP TABLE IF EXISTS numeros;
  CREATE TABLE numeros(id int PRIMARY KEY);
  
  WHILE (contador<=dato) DO 
    INSERT INTO numeros (id) VALUES (contador);
    set contador=contador+1;
  END WHILE;

END;


CALL p1(10);
SELECT * FROM numeros;



-- procedimiento p2
-- procedimiento que recibe como argumento un numero 
-- y me crea una tabla llamada numeros con los numeros desde 1 hasta el valor pasado como argumento y al lado
-- de cada numero la suma de los numeros hasta ese numero pasado siempre mayor que 1
-- aprovechar la funcion realiza para sumar los n primeros numeros
-- call p2(5) 
-- id suma
-- 1 1
-- 2 3
-- 3 6
-- 4 10
-- 5 15

DROP PROCEDURE IF EXISTS p2;
CREATE PROCEDURE p2(dato int)
BEGIN
-- declarar variables
DECLARE contador int DEFAULT 1;

DROP TABLE IF EXISTS numeros;
CREATE TABLE numeros (
  id int PRIMARY KEY,
  suma int
  );

-- procesamiento
WHILE (contador<=dato) DO 
  INSERT INTO numeros (id,suma) VALUES (contador,f1(contador));
  set contador=contador+1;
END WHILE;

END;

CALL p2(5);
SELECT * FROM numeros;

-- FUNCION LETRAS
-- LE PASAMOS A LA FUNCION UN TEXTO (FRASE)
-- Y NOS TIENE QUE DEVOLVER EL NUMERO DE VOCALES QUE CONTIENE
-- SOLAMENTE TENDREMOS VOCALES SIN TILDE
-- SIEMPRE TENDREMOS ALGO EN LA FRASE

-- 1 VERSION IF/ELSE
-- 2 VERSION CASE

-- letras("hola") --> 2


DROP FUNCTION IF EXISTS letras;
CREATE FUNCTION letras(frase varchar(100))
RETURNS int DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida int DEFAULT 0; -- numero de vocales
DECLARE contador int DEFAULT 1;
DECLARE longitud int DEFAULT 0;
DECLARE caracter char(1) default '';

-- procesamiento
-- numero de caracteres de la frase
set longitud=CHAR_LENGTH(frase);

-- cuento las vocales
WHILE (contador<=longitud) DO 
  -- leo un caracter
  SET caracter= SUBSTR(frase,contador,1);
  IF (esvocal(caracter)) THEN
     set salida=salida+1; 
  END IF;
  SET contador=contador+1;
END WHILE;

-- devolver resultado
RETURN salida;
END;

-- realizado con el if

DROP FUNCTION IF EXISTS esvocal;
CREATE FUNCTION esvocal(letra char(1))
RETURNS boolean DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida boolean DEFAULT FALSE;
-- procesamiento
IF (LOWER(letra) IN ('a','e','i','o','u')) THEN
  set salida=TRUE;
END IF;
-- devolver resultado
RETURN salida;
END;

-- realizado con case

DROP FUNCTION IF EXISTS esvocal;
CREATE FUNCTION esvocal(letra char(1))
RETURNS boolean DETERMINISTIC
BEGIN
-- declarar variables
DECLARE salida boolean DEFAULT FALSE;
-- procesamiento
CASE (LOWER(letra))
  WHEN 'a' THEN set salida=TRUE;
  WHEN 'e' THEN set salida=TRUE;
  WHEN 'i' THEN set salida=TRUE;
  WHEN 'o' THEN set salida=TRUE;
  WHEN 'u' THEN set salida=TRUE;
  ELSE set salida=FALSE;
END CASE;

-- devolver resultado
RETURN salida;
END;

SELECT letras('ejEmplo');

