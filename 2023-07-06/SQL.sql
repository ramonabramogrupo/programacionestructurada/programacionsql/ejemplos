﻿CREATE DATABASE ejemplosProgramacion;
USE ejemplosProgramacion;

/**
  La funcion 1 recibe 3 numeros y devuelve el numero menor
**/
DROP FUNCTION IF EXISTS funcion1;
CREATE FUNCTION funcion1 (numero1 int, numero2 int, numero3 int)
RETURNS int DETERMINISTIC
BEGIN
  -- declarar variables
  DECLARE resultado int DEFAULT 0;
  -- procesamiento
  SET resultado = LEAST(numero1, numero2, numero3);
  -- devolver resultado
  RETURN resultado;
END;

SELECT
  funcion1(3, 4, 5);

/**
  La funcion 1 recibe 3 numeros y devuelve el numero menor
**/

DROP FUNCTION IF EXISTS funcion2;
CREATE FUNCTION funcion2 (numero1 int, numero2 int, numero3 int)
RETURNS int DETERMINISTIC
BEGIN
  -- declarar variables
  DECLARE resultado int DEFAULT 0;
  -- procesamiento
  IF (numero1 < numero2) THEN
    IF (numero1 < numero3) THEN
      SET resultado = numero1;
    ELSE
      SET resultado = numero3;
    END IF;
  ELSE
    IF (numero2 < numero3) THEN
      SET resultado = numero2;
    ELSE
      SET resultado = numero3;
    END IF;
  END IF;
  -- devolver resultado
  RETURN resultado;
END;

SELECT
  funcion2(4, 2, 0);


DROP FUNCTION IF EXISTS funcion3;
CREATE FUNCTION funcion3 (numero1 int, numero2 int, numero3 int)
RETURNS int DETERMINISTIC
BEGIN
  -- declarar variables
  DECLARE resultado int DEFAULT 0;
  -- procesamiento
  IF (numero1 < numero2 AND numero1<numero3) THEN
    set resultado=numero1;
  ELSEIF(numero2<numero3) THEN
    set resultado=numero2;
  ELSE
    set resultado=numero3;
  END IF;
    
  -- devolver resultado
  RETURN resultado;
END;


DROP FUNCTION IF EXISTS funcion4;
CREATE FUNCTION funcion4 (numero1 int, numero2 int, numero3 int)
RETURNS int DETERMINISTIC
BEGIN
  -- declarar variables
  DECLARE resultado int DEFAULT 0;
  -- procesamiento
  
  DROP TEMPORARY TABLE IF EXISTS numeros;
  CREATE TEMPORARY TABLE numeros(
    id int AUTO_INCREMENT,
    numero int,
    PRIMARY KEY(id)
    );

  INSERT INTO numeros (numero) VALUES (numero1),(numero2),(numero3);

  -- SET resultado =(SELECT MIN(numero) FROM numeros);

  SELECT MIN(numero) INTO resultado FROM numeros;

    
  -- devolver resultado
  RETURN resultado;
END;

SELECT funcion4(4,6,9);


/**
crear procedimiento que me devuelva el numero mayor de 3 numeros
**/

DROP PROCEDURE IF EXISTS procedimiento1;
CREATE PROCEDURE procedimiento1(numero1 int,numero2 int, numero3 int, INOUT resultado int)
BEGIN
-- declarar variables

set resultado=GREATEST(numero1,numero2,numero3);

-- procesamiento

END;


CALL procedimiento1(1,5,6,@mayor);
SELECT @mayor;