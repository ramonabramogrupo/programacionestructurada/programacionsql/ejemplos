﻿USE ciclistas;

DROP FUNCTION IF EXISTS funcion1;
CREATE FUNCTION funcion1(numero1 int,numero2 int)
RETURNS int DETERMINISTIC
BEGIN
  -- declaro e inicializo la variable
  DECLARE resultado int DEFAULT 0;
  
  -- asigno la suma a resultado
  set resultado=numero1+numero2;
  -- todas las funciones deben acabar con return
  RETURN resultado;
END;

SELECT funcion1(2,3);

SELECT funcion1(edad,4) FROM ciclista c;

UPDATE ciclista c
  SET c.edad=funcion1(edad,2);


DROP PROCEDURE IF EXISTS procedimiento1;
CREATE PROCEDURE procedimiento1(numero1 int, numero2 int)
BEGIN
  DECLARE resultado int DEFAULT 0;
  set resultado=numero1+numero2;
  SELECT resultado;
END;

CALL procedimiento1(2,3);