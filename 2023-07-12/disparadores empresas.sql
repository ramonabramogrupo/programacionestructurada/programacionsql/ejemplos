﻿USE disparadores1;

# DISPARADOR
-- Crear un disparador para que cuando INSERTE un empresa calcule 
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada
-- empresasBI

DROP TRIGGER IF EXISTS empresasBI;
CREATE TRIGGER empresasBI
BEFORE INSERT
  ON empresas
  FOR EACH ROW
BEGIN
  SET LC_TIME_NAMES='es_es';
  
  SET 
    NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
    NEW.mes=MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero=MONTH(new.fechaEntrada),
    new.diaMes=DAY(new.fechaEntrada),
    new.anno=year(new.fechaEntrada);
END ;


-- INSERTAR UNA EMPRESA

INSERT INTO empresas (empresa, fechaEntrada)
  VALUES ('juguetes norma', '2021-12-4');

-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;


# DISPARADOR
-- Crear un disparador para que cuando ACTUALIZAR una empresa me coloque correctamente
-- diasemana
-- mes
-- mesNumero
-- anno
-- de la fecha de entrada
-- empresasBU

DROP TRIGGER IF EXISTS empresasBU;
CREATE TRIGGER empresasBU
BEFORE UPDATE
  ON empresas
  FOR EACH ROW
BEGIN
  SET LC_TIME_NAMES='es_es';
  
  SET 
    NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
    NEW.mes=MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero=MONTH(new.fechaEntrada),
    new.diaMes=DAY(new.fechaEntrada),
    new.anno=year(new.fechaEntrada);
END ;


-- ACTUALIZO UNA EMPRESA

UPDATE empresas e
  SET e.fechaEntrada='2021/8/14'
  WHERE e.empresa='alpe';


-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;


-- Modificar el disparador anterior para que en caso de que 
-- al actualizar la empresa no se modidique la fecha de entrada
-- no realice ningun calculo

DROP TRIGGER IF EXISTS empresasBU;
CREATE TRIGGER empresasBU
BEFORE UPDATE
  ON empresas
  FOR EACH ROW
BEGIN
  SET LC_TIME_NAMES='es_es';

  IF (NEW.fechaEntrada<>OLD.fechaEntrada) THEN
    SET 
      NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
      NEW.mes=MONTHNAME(NEW.fechaEntrada),
      NEW.mesNumero=MONTH(new.fechaEntrada),
      new.diaMes=DAY(new.fechaEntrada),
      new.anno=year(new.fechaEntrada);  
  END IF;
  
END ;


-- ACTUALIZO UNA EMPRESA 
UPDATE empresas e
  SET e.fechaEntrada='2022/7/16'
  WHERE e.empresa='carrefour';

-- COMPRUEBO SI EL DISPARADOR FUNCIONA
SELECT * FROM empresas e;


-- CREAR UN DISPARADOR PARA CLIENTES QUE CUANDO INSERTO UNO NUEVO
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2
-- ES NECESARIO PASAR EL CODIGO EN EL INSERT

DROP TRIGGER IF EXISTS clientesBI;
CREATE TRIGGER clientesBI
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN
  SET NEW.referencia=CONCAT(new.nombre,NEW.codigo);
END ;


-- introduzco un cliente
INSERT INTO clientes (codigo,nombre, empresa,fechaNacimiento)
  VALUES (100,'silvia', 'alpe', '2000-1-2');


-- compruebo
SELECT * FROM clientes c;

-- si queremos introducir variaos clientes lo tengo que realizar en varios insert
INSERT INTO clientes (codigo,nombre, empresa,fechaNacimiento)
  VALUES (410,'lorena', 'alpe', '2000-1-2'),(420,'luis', 'alpe', '2000-1-2');

-- compruebo
SELECT * FROM clientes c;


-- MODIFICAR EL DISPARADOR PARA CLIENTES QUE CUANDO INSERTO UNO NUEVO
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2
-- NO LE PASAMOS EL CODIGO EN EL INSERT
-- ATENCION : DIFICULTAD ALTA

DROP TRIGGER IF EXISTS clientesBI;
CREATE TRIGGER clientesBI
BEFORE INSERT
  ON clientes
  FOR EACH ROW
BEGIN

DECLARE numero int DEFAULT 0;
SET @@information_schema_stats_expiry=0;

IF (NEW.CODIGO=0) THEN
  SELECT 
      AUTO_INCREMENT
    INTO 
      numero
    FROM INFORMATION_SCHEMA.TABLES
      WHERE 
    table_name = 'clientes' AND TABLE_SCHEMA='disparadores1';
  
    SET NEW.referencia=CONCAT(new.nombre,numero);
ELSE 
    SET NEW.referencia=CONCAT(new.nombre,new.CODIGO);
END IF;


END ;


-- CREAR UN DISPARADOR PARA CLIENTES QUE CUANDO ACTUALIZO UN CLIENTE
-- ME CALCULE LA REFERENCIA COMO NOMBRE DEL CLIENTE JUNTO CON EL CODIGO
-- NOMBRE=ANA Y CODIGO=2 ==> REFERENCIA=ANA2
DROP TRIGGER IF EXISTS clientesBU;
CREATE TRIGGER clientesBU
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  SET NEW.referencia=CONCAT(NEW.nombre,NEW.codigo);
END ;



-- voy a crear un procedimiento
-- que me actualice los registros que esten mal calculadas las referencias
-- LLAMARLO REFERENCIAS

DROP PROCEDURE IF EXISTS referencias;
CREATE PROCEDURE referencias()
BEGIN
UPDATE clientes c
  SET c.referencia=CONCAT(c.nombre,c.codigo); 
END;

CALL referencias();

-- introduzco dos registros en un mismo insert
INSERT INTO clientes (nombre, empresa,fechaNacimiento)
  VALUES ('cesar', 'alpe', '2000-1-2');

INSERT INTO clientes (nombre, empresa,fechaNacimiento) VALUES 
  ('lolo','alpe','2001-2-3');

-- el segundo coloca mal la referencia
SELECT * FROM clientes;

-- llamo al procedimiento
CALL referencias();

-- compruebo que este bien
SELECT * FROM clientes;


-- CREAR UN DISPARADOR PARA LA TABLA CLIENTES
-- QUE CUANDO INSERTO UN CLIENTE NUEVO
-- ME COLOQUE COMO EMAIL=[REFERENCIA]@[EMPRESA].COM


-- CREAR UN DISPARADOR PARA LA TABLA CLIENTES
-- QUE CUANDO ACTUALIZO UN CLIENTE 
-- ME COLOQUE COMO EMAIL=[REFERENCIA]@[EMPRESA].COM


-- compruebos los disparadores anteriores
INSERT into clientes (nombre, empresa, fechaNacimiento)
  VALUES ('loreto','alpe','1980-8-7');

UPDATE clientes c
  set nombre="Ramon"
  where c.codigo=1;

SELECT * FROM clientes c;


-- disparador para clientes
-- cuando creo un cliente nuevo
-- tengo que actualizar el campo fechaUltimo de la empresa
-- a la que pertenece ese cliente










