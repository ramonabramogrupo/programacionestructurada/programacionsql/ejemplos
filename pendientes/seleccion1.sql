﻿USE seleccion;

/** 
  procedimiento almacenado
**/


DROP PROCEDURE IF EXISTS seleccion1;

DELIMITER //

CREATE PROCEDURE seleccion1(numero int)
BEGIN

IF (numero>0) THEN
  SELECT "mayor que 0";
END IF;


END //

DELIMITER ;

CALL seleccion1(-1);
CALL seleccion1(1);

/** 
  procedimiento almacenado
**/


DROP PROCEDURE IF EXISTS seleccion2;

DELIMITER //

CREATE PROCEDURE seleccion2(numero int)
BEGIN
IF (numero>0) THEN
  SELECT "mayor que 0" salida;
ELSE 
  SELECT "menor o igual que 0" salida;
END IF;

END //

DELIMITER ;

CALL seleccion2(-1);
call seleccion2(1);


DROP PROCEDURE IF EXISTS seleccion3;

DELIMITER //

CREATE PROCEDURE seleccion3(numero int)
BEGIN
  IF (numero>0) THEN
    SELECT "mayor que 0";
  ELSEIF (numero<0) THEN 
    SELECT "menor que 0";
  ELSE 
    SELECT "es 0";
  END IF;

END //

DELIMITER ;

CALL seleccion3(-1);
CALL seleccion3(0);
CALL seleccion3(1);

/**
funcion

** a la funcion le paso un caracter
** quiero comprobar si el caracter es una vocal
** si es una vocal que retorne el texto "vocal"
** si no es una vocal que retorne el texto "no es vocal"
** consideraciones
** no hay tildes
** puede estar en mayusculas o minusculas
**/

DROP FUNCTION IF EXISTS vocal;

DELIMITER //

CREATE FUNCTION vocal(caracter char(1))
RETURNS varchar(10) DETERMINISTIC
BEGIN
  DECLARE salida varchar(10) DEFAULT "";
  
  SET caracter=LOWER(caracter); -- colocar el caracter en minus

  IF ( caracter IN ("a","e","i","o","u") ) THEN 
    set salida="vocal";
  ELSE 
    set salida="no vocal";
  END IF;

RETURN salida; 
END //

DELIMITER ;

SELECT vocal('A');



-- UTILIZAR SQL PARA LA SOLUCION CON TABLAS

-- creo una tabla con las vocales
DROP TABLE IF EXISTS vocales;
CREATE TABLE vocales(
  id int AUTO_INCREMENT,
  nombre char(1),
  PRIMARY KEY(id)
);
INSERT INTO vocales (nombre)
  VALUES ('a'),('e'),('i'),('o'),('u');

-- solucion utilizando esa tabla

DROP FUNCTION IF EXISTS vocal;

DELIMITER //

CREATE FUNCTION vocal(caracter char(1))
RETURNS varchar(10) DETERMINISTIC
BEGIN
  DECLARE salida varchar(10) DEFAULT "";
  DECLARE numero int DEFAULT 0;

  SELECT COUNT(*) INTO numero FROM vocales WHERE nombre=caracter;

IF (numero=1) THEN
  set salida="vocal";
ELSE 
  set salida="no vocal";
END IF;

RETURN salida; 
END //

DELIMITER ;


SELECT vocal('A');


-- solucion con like

DROP FUNCTION IF EXISTS vocal;

DELIMITER //

CREATE FUNCTION vocal(caracter char(1))
RETURNS varchar(10) DETERMINISTIC
BEGIN
  DECLARE salida varchar(10) DEFAULT "";
  DECLARE numero int DEFAULT 0;

  SELECT "aeiou" LIKE CONCAT("%",caracter,"%") INTO numero;

IF (numero=1) THEN
  set salida="vocal";
ELSE 
  set salida="no vocal";
END IF;

RETURN salida; 
END //

DELIMITER ;

SELECT vocal('A');


/** 
FUNCION par

ESTA FUNCION TIENE COMO ARGUMENTO UN NUMERO ENTERO
Y RETORNA SI EL NUMERO ES PAR O NO
SI EL NUMERO ES PAR DEVUELVE "PAR"
SI EL NUMERO ES IMPAR DEVUELVE "IMPAR"
SI EL NUMERO ES CERO DEVUELVE "CERO"

**/

DROP FUNCTION IF EXISTS par;

DELIMITER //

CREATE FUNCTION par(numero int)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20) DEFAULT "";
  
  IF (numero=0) THEN
    set salida="cero";
  ELSEIF ((numero MOD 2)=0) THEN 
    set salida="par";
  ELSE 
    set salida="impar";
  END IF;

RETURN salida; 
END //

DELIMITER ;

SELECT par(0),par(1),par(2);


/**
FUNCION NUMERO
A LA FUNCION LE PASO UN NUMERO DEL 0 AL 9
Y TIENE QUE RETORNAR EL NUMERO ESCRITO COMO TEXTO

REALIZARLO UTILIZANDO CASE

**/

DROP FUNCTION IF EXISTS numero;

DELIMITER //

CREATE FUNCTION numero(numero int)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20) DEFAULT "";

  CASE numero
    WHEN 0 THEN 
      SET salida="cero";
    WHEN 1 THEN 
      SET salida="uno";
    WHEN 2 THEN 
      SET salida="dos";
    WHEN 3 THEN 
      SET salida="tres";
    WHEN 4 THEN 
      SET salida="cuatro";
    WHEN 5 THEN 
      SET salida="cinco";
    WHEN 6 THEN 
      SET salida="seis";
    WHEN 7 THEN 
      SET salida="siete";   
    WHEN 8 THEN 
      SET salida="ocho";
    WHEN 9 THEN 
      SET salida="nueve";
    ELSE 
      set salida="numero no valido";
  END CASE;
  
RETURN salida; 
END //

DELIMITER ;

SELECT numero(0),numero(10);



/** 
la misma funcion con la otra sintaxis
**/

DROP FUNCTION IF EXISTS numero;

DELIMITER //

CREATE FUNCTION numero(numero int)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20) DEFAULT "";

  CASE 
    WHEN numero=0 THEN 
      SET salida="cero";
    WHEN numero=1 THEN 
      SET salida="uno";
    WHEN numero=2 THEN 
      SET salida="dos";
    WHEN numero=3 THEN 
      SET salida="tres";
    WHEN numero=4 THEN 
      SET salida="cuatro";
    WHEN numero=5 THEN 
      SET salida="cinco";
    WHEN numero=6 THEN 
      SET salida="seis";
    WHEN numero=7 THEN 
      SET salida="siete";   
    WHEN numero=8 THEN 
      SET salida="ocho";
    WHEN numero=9 THEN 
      SET salida="nueve";
    ELSE 
      set salida="numero no valido";
  END CASE;
  
RETURN salida; 
END //

DELIMITER ;

SELECT numero(0),numero(10);


/***

PROCEDIMIENTO RELLENAR
ACTUALIZAR EL CAMPO TEXTO CON EL TEXTO DEL CAMPO VALOR

UTILIZANDO LA FUNCION NUMERO
**/

DROP TABLE IF EXISTS numeros;

CREATE TABLE numeros(
  id int AUTO_INCREMENT,
  valor int,
  texto varchar(20),
  PRIMARY KEY(id),
  CHECK(valor BETWEEN 0 AND 9)
);

INSERT INTO numeros (valor)
  VALUES (2),(3),(5),(2),(0);

SELECT * FROM numeros n;

DROP PROCEDURE IF EXISTS rellenar;

DELIMITER //

CREATE PROCEDURE rellenar()
BEGIN
  UPDATE numeros n
    set texto=numero(valor);
END //

DELIMITER ;

CALL rellenar();
SELECT * FROM numeros n;

/**
Procedimiento almacenado calculoFechas

actualiza todos los registros de la tabla fechas
colocando los campos calculados utilizando las funciones
dia  ==> dia de la semana de la fecha entrada
mes ==> mes de la fecha entrada
dias ==> dias de diferencia entre la fecha de salida y la fecha de entrada
**/

DROP TABLE IF EXISTS fechas;
CREATE TABLE fechas(
  id int AUTO_INCREMENT,
  fechaEntrada date,
  fechaSalida date,
  dias int COMMENT 'dias de diferencia entre fecha de entrada y fecha de salida', 
  mesFechaEntrada varchar(20) COMMENT 'mes de la fecha de entrada como texto',
  diaFechaEntrada varchar(20) COMMENT 'dia de la semana de la fecha de entrada como texto',
  PRIMARY KEY (id)
);

INSERT INTO fechas (fechaEntrada, fechaSalida)
  VALUES ('2022/12/01','2022/12/5'),('2022/01/01','2022/07/05');

SELECT * FROM fechas f;

/**
funcion dias

le paso dos fechas y devuelve el numero de dias entre ellas
**/

-- VERSION TIMESTAMPFIFF

DROP FUNCTION IF EXISTS dias;

DELIMITER //

CREATE FUNCTION dias(fecha1 date,fecha2 date)
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  
  SET salida=ABS(TIMESTAMPDIFF(DAY,fecha1,fecha2));

RETURN salida; 
END //

DELIMITER ;

SELECT dias('2022-1-1','2022-10-1');


-- VERSION DATEDIFF

DROP FUNCTION IF EXISTS dias;

DELIMITER //

CREATE FUNCTION dias(fecha1 date,fecha2 date)
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  
  SET salida=ABS(DATEDIFF(fecha1,fecha2));

RETURN salida; 
END //

DELIMITER ;

SELECT dias('2022-1-1','2022-10-1');

-- VERSION TO_DAYS

DROP FUNCTION IF EXISTS dias;

DELIMITER //

CREATE FUNCTION dias(fecha1 date,fecha2 date)
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  
  SET salida=ABS(TO_DAYS(fecha1)-TO_DAYS(fecha2));

RETURN salida; 
END //

DELIMITER ;

SELECT dias('2022-1-1','2022-10-1');

/**
funcion Mes

devuelve el mes como texto de una fecha que le pase
**/


-- version monthname
DROP FUNCTION IF EXISTS mes;

DELIMITER //

CREATE FUNCTION mes(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);
  set @@lc_time_names='es_es'; /* coloco en castellano */

  SET salida=MONTHNAME(fecha);
  
  
RETURN salida; 
END //

DELIMITER ;

SELECT mes('2022/11/22');

-- version month

DROP FUNCTION IF EXISTS mes;

DELIMITER //

CREATE FUNCTION mes(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);
  CASE MONTH(fecha)
   WHEN 1 THEN set salida='Enero';
   WHEN 2 THEN set salida='Febrero';
   WHEN 3 THEN set salida='Marzo';
   WHEN 4 THEN set salida='Abril';
   WHEN 5 THEN set salida='Mayo';
   WHEN 6 THEN set salida='Junio';
   WHEN 7 THEN set salida='Julio';
   WHEN 8 THEN set salida='Agosto';
   WHEN 9 THEN set salida='Septiembre';
   WHEN 10 THEN set salida='Octubre';
   WHEN 11 THEN set salida='Noviembre';
   WHEN 12 THEN set salida='Diciembre';
 END CASE;
  
RETURN salida; 
END //

DELIMITER ;

SELECT mes('2022/11/22');


-- solucion con tabla

DROP TABLE IF EXISTS meses;
CREATE TABLE meses(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  PRIMARY KEY(id)
);
INSERT INTO meses (nombre)
  VALUES ('Enero'),('Febrero'),('Marzo'),('Abril'),
  ('Mayo'),('Junio'),('Julio'),('Agosto'),('Septiembre'),('Octubre'),
  ('Noviembre'),('Diciembre');

DROP FUNCTION IF EXISTS mes;

DELIMITER //

CREATE FUNCTION mes(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);

  DROP TEMPORARY TABLE IF EXISTS meses;
  CREATE TEMPORARY TABLE meses(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    PRIMARY KEY(id)
  );

  INSERT INTO meses (nombre)
  VALUES ('Enero'),('Febrero'),('Marzo'),('Abril'),
  ('Mayo'),('Junio'),('Julio'),('Agosto'),('Septiembre'),('Octubre'),
  ('Noviembre'),('Diciembre');

  -- set salida=SELECT m.nombre FROM meses m WHERE m.id=MONTH(fecha);  
  SELECT m.nombre INTO salida FROM meses m WHERE m.id=MONTH(fecha);
RETURN salida; 
END //

DELIMITER ;

SELECT mes('2022-2-1');

/**
funcion dia

devuelve el dia de la semana como texto de una fecha que le pase
**/

-- solucion dayname
DROP FUNCTION IF EXISTS dia;

DELIMITER //

CREATE FUNCTION dia(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);

  set @@lc_time_names='es_es'; /* coloco en castellano */
  set salida=DAYNAME(fecha);

RETURN salida; 
END //

DELIMITER ;

-- solucion dayofweek

DROP FUNCTION IF EXISTS dia;

DELIMITER //

CREATE FUNCTION dia(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);

  CASE DAYOFWEEK(fecha)
    WHEN 2 THEN set salida="Lunes";
    WHEN 3 THEN set salida="Martes";
    WHEN 4 THEN set salida="Miercoles";
    WHEN 5 THEN set salida="Jueves";
    WHEN 6 THEN set salida="Viernes";
    WHEN 7 THEN set salida="Sabado";
    WHEN 1 THEN set salida="Domingo";
  END CASE;

RETURN salida; 
END //

DELIMITER ;

-- version de tabla

DROP FUNCTION IF EXISTS dia;

DELIMITER //

CREATE FUNCTION dia(fecha date)
RETURNS varchar(20) DETERMINISTIC
BEGIN
  DECLARE salida varchar(20);
  
  DROP TEMPORARY TABLE IF EXISTS diasSemana;
  CREATE TEMPORARY TABLE diasSemana(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    PRIMARY KEY(id)
  );
  INSERT INTO diasSemana (nombre)
  VALUES ('Domingo'),('Lunes'),('Martes'),('Miercoles'),
          ('Jueves'),('Viernes'),('Sabado');
 
  SELECT 
      s.nombre INTO salida 
    FROM 
      diasSemana s 
    WHERE 
      s.id=DAYOFWEEK(fecha);

RETURN salida; 
END //

DELIMITER ;


SELECT dia('2022/12/22');

/* 
  Ahora realizo el procedimiento almacenado que 
  actualiza la tabla
*/

DROP PROCEDURE IF EXISTS actualizaFechas;

DELIMITER //

CREATE PROCEDURE actualizaFechas()
BEGIN
  UPDATE fechas f
    SET 
      f.dias=dias(f.fechaEntrada,f.fechaSalida),
      f.mesFechaEntrada=mes(f.fechaEntrada),
      f.diaFechaEntrada=dia(f.fechaEntrada);
      
END //

DELIMITER ;

CALL actualizaFechas();
SELECT * FROM fechas f;