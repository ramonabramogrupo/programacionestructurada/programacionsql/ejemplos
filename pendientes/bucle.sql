﻿CREATE DATABASE bucles;
USE bucles;


-- procedimiento

-- 

DROP PROCEDURE IF EXISTS p1;

DELIMITER //

CREATE PROCEDURE p1()
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE acumulador int DEFAULT 0;

WHILE (contador<10) DO 
  set contador=contador+1;
  set acumulador=acumulador+contador;
END WHILE;

  SELECT acumulador,contador;

END //

DELIMITER ;

CALL p1();


-- funcion f1
-- Función que recibe como argumento un numero 
-- y me devuelve la suma de todos los números enteros desde 1 hasta el numero pasado
-- Numero pasado siempre mayor que 1

DROP FUNCTION IF EXISTS f1;

DELIMITER //

CREATE FUNCTION f1(numero int)
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  DECLARE contador int DEFAULT 1;
  
  WHILE (contador<=numero) DO 
    set salida=salida+contador;
    set contador=contador+1;
  END WHILE;
  
RETURN salida; 
END //

DELIMITER ;

SELECT f1(2);

-- FUNCION f2
-- REALIZAR UNA FUNCION QUE ME DEVUELVA LA SUMA DE LOS
-- NUMEROS PARES ENTRE 1 Y EL NUMERO QUE LE PASEMOS COMO ARGUMENTO
-- EL NUMERO PASADO SIEMPRE SERA MAYOR QUE 1

DROP FUNCTION IF EXISTS f2;

DELIMITER //

CREATE FUNCTION f2(numero int)
RETURNS int DETERMINISTIC
BEGIN
  DECLARE acumulador int DEFAULT 0;
  DECLARE contador int DEFAULT 2;
  WHILE (contador<=numero) DO 
    SET acumulador=acumulador+contador;
    SET contador=contador+2;
  END WHILE;
 

RETURN acumulador; 
END //

DELIMITER ;

SELECT f2(8);

-- PROCEDIMIENTO LETRAS
-- LE PASAMOS AL PROCEDIMIENTO UN TEXTO (FRASE)
-- Y NOS TIENE QUE MOSTRAR EL NUMERO DE VOCALES QUE CONTIENE
-- SOLAMENTE TENDREMOS VOCALES SIN TILDE
-- SIEMPRE TENDREMOS ALGO EN LA FRASE

DROP PROCEDURE IF EXISTS letras;

DELIMITER //

CREATE PROCEDURE letras(frase varchar(100))
BEGIN
  DECLARE salida int DEFAULT 0; -- NUMERO DE VOCALES
  DECLARE contador int DEFAULT 1; 
  DECLARE longitud int DEFAULT 0;
  DECLARE caracter char(1) DEFAULT '';
  DECLARE esVocal int DEFAULT 0;

  SET longitud=CHAR_LENGTH(frase); -- LEO LA LONGITUD DE LA FRASE PASADA

  WHILE (contador<=longitud) DO 
    SET caracter=SUBSTRING(frase,contador,1); -- leo el caracter
    set esVocal=vocales(caracter); -- compruebo si el caracter leido es una vocal
    SET salida=salida+esVocal;
    set contador=contador+1;
  END WHILE;
  
  SELECT salida;

END //

DELIMITER ;

CALL letras('ejemplO de clase');

-- CREAR LA FUNCION VOCALES

DROP FUNCTION IF EXISTS vocales;

DELIMITER //

CREATE FUNCTION vocales(caracter char(1))
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  
  CASE LOWER(caracter)
    WHEN 'a' THEN SET salida=1;
    WHEN 'e' THEN SET salida=1;
    WHEN 'i' THEN SET salida=1;
    WHEN 'o' THEN SET salida=1;
    WHEN 'u' THEN SET salida=1;
    ELSE set salida=0;
  END CASE;

RETURN salida; 
END //

DELIMITER ;

-- funcion vocales de otra forma

DROP FUNCTION IF EXISTS vocales;

DELIMITER //

CREATE FUNCTION vocales(caracter char(1))
RETURNS int DETERMINISTIC
BEGIN
  DECLARE salida int DEFAULT 0;
  
  IF (caracter IN('a','e','i','o','u') ) THEN
    set salida=1;
  ELSE 
    set salida=0;
  END IF;

RETURN salida; 
END //

DELIMITER ;

SELECT SUBSTRING('frase',5,1);



