﻿USE procedimientosfunciones1;

CREATE OR REPLACE TABLE numeros(
  valor int
);

INSERT INTO numeros VALUES
  (1),(100),(220),(400),(1000);



-- quiero un procedimiento que me permita indicar
-- cuantos numeros pares hay dentro de la tabla

DELIMITER //
CREATE OR REPLACE PROCEDURE pares()
BEGIN
  SELECT COUNT(*) FROM numeros WHERE MOD(valor,2)=0;
END //
DELIMITER ;

CALL pares();

DELIMITER //
CREATE OR REPLACE PROCEDURE pares1()
BEGIN
  -- esta variable me ayuda para saber si estoy al final
  -- de la tabla cuando leo con el cursor
  DECLARE final int DEFAULT 0;

  -- variable para utilizar con el cursor
  DECLARE valorLeido int;

  -- en esta variable nos va a colocar el resultado
  DECLARE contadorPares int DEFAULT 0;

  -- declaro el cursor
  DECLARE c CURSOR FOR 
    SELECT valor FROM numeros;

  -- control de excepciones
  -- manejar la excepcion de final de tabla

  DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET final=1;

  -- inicializamos el cursor
  OPEN c;
  -- leo el primer registro
  FETCH c INTO valorLeido;
  WHILE (final=0) DO
    IF (MOD(valorLeido,2)=0) THEN
      SET contadorPares=contadorPares+1;
    END IF;
    
    FETCH c INTO valorLeido;
  END WHILE;
-- cerrar el cursor
CLOSE c;
  SELECT contadorPares;
END //
DELIMITER ;
TRUNCATE numeros;
CALL pares1();

-- quiero encontrar en la tabla numeros los registros que
-- sean mayores que 5 y almacenar en una tabla nueva 
-- esos numeros

  CREATE OR REPLACE TABLE nuevaNumeros(
    valor int);
  TRUNCATE nuevaNumeros;

  DELIMITER //
  CREATE OR REPLACE PROCEDURE numeros5 ()
  BEGIN
    /* variables */
    DECLARE final int DEFAULT 0;
    DECLARE valorLeido int;

    /* cursores */
    DECLARE a CURSOR FOR 
      SELECT valor FROM numeros;

    /* excepciones */
    DECLARE CONTINUE HANDLER FOR NOT FOUND
      set final=1;
    /* programa */
    OPEN a;
    FETCH a INTO valorLeido;
    WHILE(final=0) DO
      IF(valorLeido>5) THEN
        INSERT INTO nuevaNumeros VALUE (valorLeido);
      END IF;
      FETCH a INTO valorLeido;
    END WHILE;
    CLOSE a;
  END //
  DELIMITER ;

CALL numeros5();
SELECT * FROM nuevaNumeros n;

-- tengo una tabla como la siguiente

CREATE OR REPLACE TABLE notas(
  id int AUTO_INCREMENT,
  nota int,
  repeticiones int,
  valor varchar(50),
  PRIMARY KEY(id)
  );

INSERT INTO notas(nota) VALUES
  (5),(5),(3),(7),(9),(9),(10),(5),(1),(5),(3),(9);

-- repeciciones: numero de veces que se repite esa nota 
-- hasta el valor leido
-- valor: la nota pero con texto
-- 0,5 : suspenso
-- 5,6 : suficiente
-- 6,7 : bien
-- 7,9: notable
-- 9,10: sobresaliente

DELIMITER //
CREATE OR REPLACE PROCEDURE calculoNotas()
BEGIN
/* variables */
  DECLARE final int DEFAULT 0;
  DECLARE notaLeida int;
  DECLARE registro int;
  DECLARE textoNota varchar(50);
  DECLARE r0 int DEFAULT 0;
  DECLARE r1 int DEFAULT 0;
  DECLARE r2 int DEFAULT 0;
  DECLARE r3 int DEFAULT 0;
  DECLARE r4 int DEFAULT 0;
  DECLARE r5 int DEFAULT 0;
  DECLARE r6 int DEFAULT 0;
  DECLARE r7 int DEFAULT 0;
  DECLARE r8 int DEFAULT 0;
  DECLARE r9 int DEFAULT 0;
  DECLARE r10 int DEFAULT 0;
  DECLARE repeticiones int;

/* cursores */
  DECLARE c CURSOR FOR 
    SELECT id,nota FROM notas;
  

/* excepciones */
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET final=1;

/* programa */
  -- abrimos cursor
  OPEN c;
  -- leemos la primera nota
  FETCH c INTO registro,notaLeida;

  WHILE (final=0) DO
    
    -- calculo el texto de la nota
    SET textoNota=notaTexto(notaLeida);

    -- calcular las repeticiones
    CASE notaLeida
      WHEN 0 THEN 
        set repeticiones=r0;
        set r0=r0+1;
      WHEN 1 THEN 
        set repeticiones=r1;
        set r1=r1+1;
      WHEN 2 THEN 
        set repeticiones=r2;
        set r2=r2+1;
      WHEN 3 THEN 
        set repeticiones=r3;
        set r3=r3+1;
      WHEN 4 THEN 
        set repeticiones=r4;
        set r4=r4+1;
      WHEN 5 THEN 
        set repeticiones=r5;
        set r5=r5+1;
      WHEN 6 THEN 
        set repeticiones=r6;
        set r6=r6+1;
      WHEN 7 THEN 
        set repeticiones=r7;
        set r7=r7+1;
      WHEN 8 THEN 
        set repeticiones=r8;
        set r8=r8+1;
      WHEN 9 THEN 
        set repeticiones=r9;
        set r9=r9+1;
      ELSE 
        set repeticiones=r10;
        set r10=r10+1;
    END CASE;
    
    UPDATE notas n 
      SET 
        n.valor=textoNota,
        n.repeticiones=repeticiones
      WHERE 
        n.id=registro;

    -- leemos la nota siguiente
    FETCH c INTO registro,notaLeida;
  END WHILE;

  CLOSE c;
END //
DELIMITER ;

CALL calculoNotas();
SELECT * FROM notas n;

DELIMITER //
CREATE OR REPLACE FUNCTION notaTexto(valor int)
RETURNS varchar(50)
BEGIN
  DECLARE resultado varchar(50);
  CASE 
    WHEN (valor<5)  THEN 
      set resultado='Suspenso';
    WHEN (valor<6) THEN 
      set resultado='Suficiente';
    WHEN (valor<7) THEN 
      set resultado='Bien';
    WHEN (valor<9) THEN 
      set resultado='Notable';
    ELSE
      set resultado='Sobresaliente'; 
  END CASE;
  RETURN resultado;
END //
DELIMITER ;

SELECT notaTexto(9);


-- Realizar el ejercicio con tabla temporal 

DELIMITER //
CREATE OR REPLACE PROCEDURE calculoNotas()
BEGIN
/* variables */
  DECLARE final int DEFAULT 0;
  DECLARE notaLeida int;
  DECLARE registro int;
  DECLARE textoNota varchar(50);
  DECLARE repeticiones int;

/* cursores */
  DECLARE c CURSOR FOR 
    SELECT id,nota FROM notas;


/* excepciones */
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET final=1;

/* programa */
  -- abrimos cursor
  OPEN c;
  -- leemos la primera nota
  FETCH c INTO registro,notaLeida;

  CREATE OR REPLACE TEMPORARY TABLE valores(
      nota int PRIMARY KEY,
      repeticion int DEFAULT 0);

  WHILE (final=0) DO
    
    -- calculo el texto de la nota
    SET textoNota=notaTexto(notaLeida);
    
    INSERT INTO valores VALUE (notaLeida,0) 
      ON DUPLICATE KEY UPDATE repeticion=repeticion+1;
        
    UPDATE notas n 
      SET 
        n.valor=textoNota,
        n.repeticiones=(SELECT repeticion FROM valores WHERE nota=notaLeida)
      WHERE 
        n.id=registro;

    -- leemos la nota siguiente
    FETCH c INTO registro,notaLeida;
  END WHILE;

  CLOSE c;
END //
DELIMITER ;

CALL calculoNotas();


-- errores nuestros

CREATE OR REPLACE TABLE fechas(
  fecha date,
  dia varchar(20) DEFAULT 'desconocido'
);

INSERT INTO fechas(fecha) VALUES ('2000/1/1');

DELIMITER //
CREATE OR REPLACE PROCEDURE calculoDia ()
BEGIN
/* variables */
  DECLARE final boolean DEFAULT FALSE;

/* cursores */
  DECLARE c CURSOR FOR
    SELECT fecha FROM fechas;


/* excepciones */
  DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET final=TRUE;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000' 
    BEGIN 
    END;
/* programa */


END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE FUNCTION queDia (fecha date)
RETURNS varchar(255) 
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000'
    RETURN 'Domingo';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
    RETURN 'Lunes';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01002'
    RETURN 'Martes';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01003'
    RETURN 'Miercoles';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01004'
    RETURN 'Jueves';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01005'
    RETURN 'Viernes';
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01006'
    RETURN 'Sabado';
    
  
  CASE DAYOFWEEK(fecha)
      WHEN 1 THEN SIGNAL SQLSTATE '01001';
      WHEN 2 THEN SIGNAL SQLSTATE '01002';
      WHEN 3 THEN SIGNAL SQLSTATE '01003';
      WHEN 4 THEN SIGNAL SQLSTATE '01004';
      WHEN 5 THEN SIGNAL SQLSTATE '01005';
      WHEN 6 THEN SIGNAL SQLSTATE '01006';
      WHEN 7 THEN SIGNAL SQLSTATE '01007';
    END CASE;
END //
DELIMITER ;

SELECT queDia('2019/2/20');
