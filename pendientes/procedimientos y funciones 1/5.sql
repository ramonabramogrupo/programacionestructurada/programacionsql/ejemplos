﻿USE tema3;

CREATE OR REPLACE TABLE salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int DEFAULT 0
  );

CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
  );


-- creando el disparador para la tabla salas antes de insertar
DELIMITER //
CREATE OR REPLACE TRIGGER salasT1 
BEFORE INSERT
  ON salas
  FOR EACH ROW
BEGIN
-- disparador para que me calcule la edad de la sala en funcion
-- de su fecha de alta

  SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  
END //

DELIMITER ;

-- para probar mi disparador
INSERT INTO salas (butacas, fecha)
  VALUES (150, '2000/1/1');

SELECT * FROM salas s;

DELIMITER //
CREATE OR REPLACE TRIGGER salasT2 
BEFORE UPDATE
  ON salas
  FOR EACH ROW
BEGIN
-- disparador para que me calcule la edad de la sala en funcion
-- de su fecha de alta

  SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  
END //

DELIMITER ;

UPDATE salas s set s.fecha='2001/1/1';
SELECT * FROM SALAS;



DELIMITER //
CREATE OR REPLACE TRIGGER ventasT1 
BEFORE INSERT
  ON ventas
  FOR EACH ROW
BEGIN
-- quiero comprobar que la sala este en la tabla salas
  DECLARE numero int DEFAULT 0;

  SELECT COUNT(*) INTO numero 
    FROM salas WHERE id=new.sala;
  
  IF(numero=0) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la sala no existe';
  END IF;
  
END //

DELIMITER ;


INSERT INTO ventas (sala, numero)
  VALUES (1,2);

SELECT * FROM salas;
SELECT * FROM ventas;


DELIMITER //
CREATE OR REPLACE TRIGGER ventasT2 
BEFORE UPDATE 
  ON ventas
  FOR EACH ROW
BEGIN
-- quiero comprobar que la sala este en la tabla salas
  DECLARE numero int DEFAULT 0;

  SELECT COUNT(*) INTO numero 
    FROM salas WHERE id=new.sala;
  
  IF(numero=0) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la sala no existe';
  END IF;
  
END //

DELIMITER ;