﻿DROP DATABASE IF EXISTS ejemploDisparadores;
CREATE DATABASE ejemploDisparadores;
USE ejemploDisparadores;

CREATE TABLE productos(
  producto varchar(5),
  precio float,
  cantidad int DEFAULT 0,
  codigoEspecial varchar(20) DEFAULT NULL,
  inicialProducto char(1) DEFAULT NULL,
  fechaUltimaVenta datetime,
  PRIMARY KEY(producto)
  );

INSERT INTO productos (producto,precio)
  VALUES ('p1',10),('p2',1),('p3',2),('p4',22),('p5',8);


CREATE TABLE ventas (
  id int(11) AUTO_INCREMENT,
  producto varchar(5) DEFAULT NULL,
  unidades int(11) NOT NULL,
  total int(11) DEFAULT 0,
  fechaVenta datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT fkventasproductos FOREIGN KEY (producto) REFERENCES productos(producto) ON DELETE RESTRICT on UPDATE RESTRICT
);


INSERT INTO ventas (producto, unidades) VALUES
('p1', 10),
('p2', 20),
('p1', 70),
('p3', 210),
('p1', 80),
('p3', 30),
('p4', 16),
('p5',10);

SELECT * FROM ventas v;