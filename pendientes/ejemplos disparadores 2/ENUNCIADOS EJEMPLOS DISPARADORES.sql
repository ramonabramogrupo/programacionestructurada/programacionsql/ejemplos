﻿USE ejemploDisparadores;
/***
Crear una instruccion de actualizacion para calcular el total

**/




/**
  Ejercicio 1
  Crear un disparador para la tabla ventas para que cuando metas un registro 
  nuevo te calcule el total automáticamente
**/

-- insertar una nueva venta


-- comprobar que el total es correcto


/**
Ejercicio 2
Crear un disparador para la tabla productos 
para que cuando inserte un producto nuevo me calcule
CODIGOESPECIAL COMO LA UNION DEL NOMBRE DEL PRODUCTO EN MAYUSCULAS Y EL PRECIO DEL PRODUCTO 
  SEPARADOS POR UN GUION
INICIALPRODUCTO COMO EL PRIMER CARACTER DEL NOMBRE DEL PRODUCTO
**/


-- insertar un producto nuevo

-- comprobar


/**
  Ejercicio 3
  Crear un disparador para la tabla ventas para que cuando inserte un registro 
  me reste las unidades de la tabla productos (en el campo cantidad).
**/

-- insertar un venta y comprobar el disparador

-- comprobar que la venta es correcta

-- comprobar el funcionamiento del disparador


/**
  Ejercicio 4
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
**/


-- actualizar una venta

-- compobar ventas

/**
  Ejercicio 5
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  me actualice la inicialProducto y el codigoEspecial
**/


/**
  Ejercicio 6
  Crear un disparador para la tabla ventas para que cuando actualice un registro 
  me reste las unidades a la cantidad en la tabla productos
  Tener en cuenta si cambiamos tambien el codigo de producto en ventas
  Suponer que el codigo del producto introducido en ventas siempre exista en la tabla productos
**/


-- actualizar una venta


-- compobar ventas


/**
  Ejercicio 7
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te sume las unidades al campo cantidad de la tabla 
  productos 
**/



/** 
Ejercicio 8
  Crear un disparador para la tabla ventas para que cuando inserte un venta
  me actualice el campo de fechaUltimaVEnta del campo productos
  Debe colocarme como fechaUltimaVenta la fecha actual en el producto que estoy vendiendo
**/
