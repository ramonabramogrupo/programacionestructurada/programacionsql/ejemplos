﻿USE ejemploDisparadores;
/***
Crear una instruccion de actualizacion para calcular el total
**/

UPDATE ventas v JOIN productos p ON v.producto = p.producto
  SET v.total=p.precio*v.unidades;

SELECT * FROM ventas v;


/**
  Ejercicio 1
  Crear un disparador para la tabla ventas para que cuando metas un registro 
  nuevo te calcule el total automáticamente
**/

DROP TRIGGER IF EXISTS biventas;
DELIMITER //
CREATE TRIGGER biventas
	BEFORE INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  DECLARE vPrecio float DEFAULT 0;

SELECT p.precio INTO vPrecio FROM productos p WHERE p.producto=NEW.producto;
  -- cuando metas un registro 
  -- nuevo te calcule el total automáticamente
    SET NEW.total=new.unidades*VPrecio;
END //

DELIMITER ;

-- insertar una nueva venta
INSERT INTO ventas (producto, unidades)
  VALUES ('p1', 100);

-- comprobar que el total es correcto
SELECT * FROM ventas v;

/**
  Ejercicio 2
  Crear un disparador para la tabla ventas para que cuando inserte un registro 
  me reste las unidades de la tabla productos (en el campo cantidad).
**/
DROP TRIGGER IF EXISTS aiventas; 
DELIMITER //
CREATE TRIGGER aiventas
	AFTER INSERT
	ON ventas
	FOR EACH ROW
BEGIN
    UPDATE productos p SET p.cantidad=p.cantidad-new.unidades 
      WHERE p.producto=new.producto;
END //
DELIMITER ;

-- insertar un venta y comprobar el disparador
INSERT INTO ventas (producto, unidades)
  VALUES ('p1', 1);

-- comprobar que la venta es correcta
SELECT * FROM ventas v;
-- comprobar el funcionamiento del disparador
SELECT * FROM productos p;

/**
  Ejercicio 3
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
**/

DROP TRIGGER IF EXISTS buventas;
DELIMITER //
CREATE TRIGGER buventas
	BEFORE UPDATE
	ON ventas
	FOR EACH ROW
BEGIN
 DECLARE vPrecio float DEFAULT 0;

  SELECT p.precio INTO vPrecio FROM productos p WHERE p.producto=NEW.producto;
  SET NEW.total=new.unidades*VPrecio;
  
END //
DELIMITER ;

-- actualizar una venta
UPDATE ventas v
  SET v.unidades=100
  WHERE v.id=1;

-- compobar ventas
SELECT * FROM ventas v;
SELECT * FROM productos p;


/**
  Ejercicio 4
  Crear un disparador para la tabla ventas para que cuando actualice un registro 
  me reste las unidades a la cantidad en la tabla productos
  Tener en cuenta si cambiamos tambien el codigo de producto en ventas
  Suponer que el codigo del producto introducido en ventas siempre exista en la tabla productos
**/

  DROP TRIGGER IF EXISTS auventas;
  DELIMITER //
  CREATE TRIGGER auventas
  	AFTER UPDATE
  	ON ventas
  	FOR EACH ROW
  BEGIN
    IF(new.producto=old.producto) THEN 
      UPDATE productos p 
        SET p.cantidad=p.cantidad-(new.unidades-old.unidades) 
        WHERE p.producto=new.producto;
    ELSE
      UPDATE productos p
        SET p.cantidad=p.cantidad+old.unidades
        WHERE p.producto=old.producto;
      UPDATE productos p
        SET p.cantidad=p.cantidad-new.unidades
        WHERE p.producto=new.producto;
     END IF;
  END //
  DELIMITER ;

-- actualizar una venta
UPDATE ventas v
  SET v.producto='p2',v.unidades=100
  WHERE v.id=1;

-- compobar ventas
SELECT * FROM ventas v;
SELECT * FROM productos p;

/**
  Ejercicio 5
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te sume las unidades al campo cantidad de la tabla 
  productos 
**/

DROP TRIGGER IF EXISTS adventas;
DELIMITER //
CREATE TRIGGER adventas
	AFTER DELETE
	ON ventas
	FOR EACH ROW
BEGIN
    UPDATE productos 
      SET cantidad=cantidad+old.unidades
      WHERE producto=old.producto;
END //

DELIMITER ;

