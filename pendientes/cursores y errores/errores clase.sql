﻿DROP DATABASE IF EXISTS excepciones;
CREATE DATABASE excepciones;
USE excepciones;

-- tabla para introducir un texto cuando ocurra un error

CREATE TABLE errores(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  fecha datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
  );

/**
  MANIPULADORES
  CONDICIONES
  SEÑALES
**/


/**
  CONTROLAR TODOS LOS ERRORES DE TIPO EXCEPTION
  Es equivalente a indicar todos los valores de SQLSTATE 
  que empiezan por 00, 01 y 02.
 **/

DELIMITER //
DROP PROCEDURE IF EXISTS error1 //
CREATE PROCEDURE error1()
BEGIN
/** manipuladores **/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      INSERT INTO errores (nombre) VALUES ('error en procedimiento 1');
    END;

/** codigo **/
  SELECT * FROM ejemplo;
    
END //
DELIMITER ;

CALL error1();
SELECT * FROM errores e;

/**
  controlar un error 1051
  intento eliminar una tabla que no existe
**/

  DELIMITER //
  DROP PROCEDURE IF EXISTS error2 //
  CREATE PROCEDURE error2()
  BEGIN
  /** manipuladores **/
    DECLARE CONTINUE HANDLER FOR 1051
      BEGIN
      INSERT INTO errores (nombre) VALUES ('error en el procedimiento 2');
      END;
  /** codigo **/
    DROP TABLE ejemplo; -- intentando eliminar una tabla que no existe
      
  END //
  DELIMITER ;

CALL error2();
SELECT * FROM errores e;

/**
  Quiero controlar el error 1051
  Pero utilizando una condicion
  Una condicion es dar un nombre a un tipo de error
  **/

  DELIMITER //
  DROP PROCEDURE IF EXISTS error3 //
  CREATE PROCEDURE error3()
  BEGIN
  /** condiciones **/
    DECLARE tablaNoExiste CONDITION FOR 1051;
  /** manipuladores **/
    DECLARE CONTINUE HANDLER FOR tablaNoExiste
      BEGIN 
        INSERT INTO errores (nombre) VALUES ('error en el procedimiento 3');
      END;
  /** codigo **/
      DROP TABLE ejemplo;
      DROP TABLE ejemplo1;
      DELETE FROM ejemplo2; -- error no controlado
  END //
  DELIMITER ;

CALL error3();
SELECT * FROM errores e;

/**
  control de errores utilizando exception
  con continue
 **/

DELIMITER //
DROP PROCEDURE IF EXISTS error4 //
CREATE PROCEDURE error4()
BEGIN
/** manipulador **/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      INSERT INTO numeros (id) VALUES (RAND()*10000);
    END;

/** codigo **/
    CREATE TABLE numeros(id int PRIMARY KEY); -- produce el primer error
    INSERT INTO numeros VALUES(1); -- produce el segundo error
END //
DELIMITER ;

CALL error4();
SELECT * FROM numeros n;
DROP TABLE numeros;

/**
  control de errores utilizando exception
  con exit
 **/

DELIMITER //
DROP PROCEDURE IF EXISTS error5 //
CREATE PROCEDURE error5()
BEGIN
/** manipulador **/
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      INSERT INTO numeros (id) VALUES (RAND()*10000);
    END;

/** codigo **/
    CREATE TABLE numeros(id int PRIMARY KEY); -- produce el primer error
    -- todo el codigo que viene a continuacion no se ejecuta cuando 
    -- hay error
    INSERT INTO numeros VALUES(1);
END //
DELIMITER ;

CALL error5();
SELECT * FROM numeros n;
DROP TABLE numeros;


/** 
  Controlar error sqlstate '23000'  Can't write; duplicate key in table '%s'
  Controlar error 1050 Table '%s' already exists
 **/

  DELIMITER //
  DROP PROCEDURE IF EXISTS error6 //
  CREATE PROCEDURE error6()
  BEGIN
      
  /** manipuladores **/
      -- cuando la tabla ya existe introduce el error en la tabla errores
      DECLARE CONTINUE HANDLER FOR 1050 
        BEGIN 
          INSERT INTO errores(nombre) VALUE ('tabla existe en error6');
        END;
      
    -- cuando el registro ya existe introduce un registro aleatorio
    DECLARE CONTINUE HANDLER FOR SQLSTATE '23000'
      BEGIN 
        INSERT INTO numeros(numero) VALUE (RAND()*1000);
      END;
    
  /** codigo **/
    CREATE TABLE numeros(
      id int AUTO_INCREMENT,
      numero int UNIQUE,
      PRIMARY KEY(id));

    INSERT INTO numeros (numero) VALUES (1); 
  END //
  DELIMITER ;



/**
  en el procedimiento colocar una condicion para cada manipulador
  **/

  DELIMITER //
  DROP PROCEDURE IF EXISTS error7 //
  CREATE PROCEDURE error7()
  BEGIN

   /** condiciones de error **/
    DECLARE tablaExiste CONDITION FOR 1050;
    DECLARE duplicado CONDITION FOR SQLSTATE '23000';
          
  /** manipuladores **/
      -- cuando la tabla ya existe introduce el error en la tabla errores
      DECLARE CONTINUE HANDLER FOR  tablaExiste 
        BEGIN 
          INSERT INTO errores(nombre) VALUE ('tabla existe en error6');
        END;
      
    -- cuando el registro ya existe introduce un registro aleatorio
    DECLARE CONTINUE HANDLER FOR duplicado
      BEGIN 
        INSERT INTO numeros(numero) VALUE (RAND()*1000);
      END;
    
  /** codigo **/
    CREATE TABLE numeros(
      id int AUTO_INCREMENT,
      numero int UNIQUE,
      PRIMARY KEY(id));

    INSERT INTO numeros (numero) VALUES (1); 
  END //
  DELIMITER ;

CALL error7();
DROP TABLE numeros;
SELECT * FROM numeros;
SELECT * FROM errores e;



DELIMITER //
DROP PROCEDURE IF EXISTS error8 //
CREATE PROCEDURE error8()
BEGIN
/** variables **/
  DECLARE correcto int DEFAULT 1; -- variable llave

/** manipulador **/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET correcto=0; -- cuando hay error la variable se coloca a 0

  SELECT * FROM ejemplo;

  IF (correcto=0) THEN 
    SELECT 'mal';
  ELSE
    SELECT 'bien';
  END IF;
  
/** codigo **/
    
END //
DELIMITER ;

CALL error8();


SELECT error9('2022/05/21');

DELIMITER //
  DROP FUNCTION IF EXISTS error9//
  CREATE FUNCTION error9(fecha date)
    RETURNS varchar(20)
    BEGIN
    /** variables **/
    DECLARE resultado varchar(20);
    /** condicion **/
    DECLARE laboral CONDITION FOR SQLSTATE '45001';
    DECLARE festivo CONDITION FOR SQLSTATE '45000';


    /**manipuladores**/
    DECLARE CONTINUE HANDLER FOR festivo
      BEGIN
        SET resultado="festivo";
      END;

    DECLARE CONTINUE HANDLER FOR laboral
      BEGIN
        set resultado="laboral";
      END; 

    /** programa **/
    -- opcion 1
    /*CASE (DAYOFWEEK(fecha))
      WHEN 1 THEN SIGNAL festivo;
      WHEN 7 THEN SIGNAL festivo;
      ELSE SIGNAL laboral;
    END CASE;*/

    -- opcion 2
    CASE 
      WHEN (DAYOFWEEK(fecha)=1 OR DAYOFWEEK(fecha)=7)  THEN SIGNAL festivo;
      WHEN (DAYOFWEEK(fecha)>=2 AND DAYOFWEEK(fecha)<7) THEN SIGNAL laboral;
    END CASE;
    

    /** retorna **/
    RETURN resultado;
    END //
  DELIMITER ;

-- otro forma
DROP FUNCTION IF EXISTS error9;
CREATE FUNCTION error9 (fecha date)
RETURNS varchar(255) 
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000'
  BEGIN
    RETURN 'laboral';
  END;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
  BEGIN
    RETURN 'festivo';
  END;

    CASE DAYOFWEEK(fecha)
      WHEN 1 THEN SIGNAL SQLSTATE '01001';
      WHEN 2 THEN SIGNAL SQLSTATE '01000';
      WHEN 3 THEN SIGNAL SQLSTATE '01000';
      WHEN 4 THEN SIGNAL SQLSTATE '01000';
      WHEN 5 THEN SIGNAL SQLSTATE '01000';
      WHEN 6 THEN SIGNAL SQLSTATE '01000';
      WHEN 7 THEN SIGNAL SQLSTATE '01001';
      ELSE 
      RETURN 'ninguno';
    END CASE;

END;

  SELECT error9("2022/05/21");


CREATE PROCEDURE error10 (divisor int)
BEGIN
  IF (divisor = 0) THEN
    SIGNAL SQLSTATE '04012' SET MESSAGE_TEXT = ' no podemos poner un cero para dividir';
  END IF;

  SELECT
    5 / divisor;
END;

CALL error10(0);


-- puedo colocar una condicion para nombrar la excepcion

DROP PROCEDURE IF EXISTS error11;
CREATE PROCEDURE error11 (divisor int)
BEGIN
  DECLARE dividirPorCero CONDITION FOR SQLSTATE '04000';
  IF (divisor = 0) THEN
    SIGNAL dividirPorCero SET MESSAGE_TEXT = ' no podemos poner un cero para dividir', MYSQL_ERRNO = 5;
  END IF;

  SELECT
    5 / divisor;
END;

-- al controlar el error con el manejador ya no se produce el error 
-- y por tanto no hay mensaje de error
DROP PROCEDURE IF EXISTS error12;
CREATE PROCEDURE error12 (divisor int)
BEGIN
  DECLARE dividirPorCero CONDITION FOR SQLSTATE '04000';
  DECLARE EXIT HANDLER FOR dividirPorCero SELECT "infinito";
  IF (divisor = 0) THEN
    SIGNAL dividirPorCero SET MESSAGE_TEXT = ' no podemos poner un cero para dividir', MYSQL_ERRNO = 5;
  END IF;

  SELECT
    5 / divisor;
END;

  CALL error11(0);
SHOW ERRORS;
SHOW WARNINGS;

