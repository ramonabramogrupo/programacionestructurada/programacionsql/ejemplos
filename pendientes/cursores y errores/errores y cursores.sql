﻿DROP DATABASE IF EXISTS excepciones;
CREATE DATABASE excepciones;
USE excepciones;

  CREATE TABLE linea(
    id int AUTO_INCREMENT,
    factura int,
    producto int,
    precio float,
    unidades int,
    total float,
    PRIMARY KEY(id)
    );

  CREATE TABLE errores(
    id int AUTO_INCREMENT,
    descripcion varchar(500),
    tabla varchar(100),
    campo varchar(100),
    fecha datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
    );




-- la primera instruccion produce error y ya no se ejecutan mas
-- porque no controlo la excepcion
DROP PROCEDURE IF EXISTS p1  ;
CREATE PROCEDURE  p1 ()
BEGIN
/** variables **/
  

/** codigo **/
    SELECT * FROM tabla;
    SELECT * FROM errores;
    SELECT * FROM linea l;
END ;

CALL p1();

-- la primera instruccion produce error y al controlarla 
-- se ejecutan el resto de instrucciones
DROP PROCEDURE IF EXISTS p1  ;
CREATE PROCEDURE  p1 ()
BEGIN
/** variables **/
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SELECT 'error';

/** codigo **/
    SELECT * FROM tabla;
    SELECT * FROM errores;
    SELECT * FROM linea l;
END ;

CALL p1();

-- la primera instruccion produce error y lo controlo 
-- el resto de instrucciones no se ejecutan porque he colocado EXIT
DROP PROCEDURE IF EXISTS p1  ;
CREATE PROCEDURE  p1 ()
BEGIN
/** variables **/
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN 
    GET DIAGNOSTICS CONDITION 1
         @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
          SELECT @p1, @p2;
    
    END;

/** codigo **/
    SELECT * FROM tabla;
    SELECT * FROM errores;
    SELECT * FROM linea l;
END ;

CALL p1();








      SELECT @@sql_notes;

      SELECT * FROM errores e;
      SHOW ERRORS;
      SHOW WARNINGS \G;
