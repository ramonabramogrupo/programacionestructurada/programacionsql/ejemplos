﻿USE piezas;

SELECT * FROM j;
SELECT * FROM p;
SELECT * FROM s;

DELIMITER //
DROP PROCEDURE IF EXISTS p1 //
CREATE PROCEDURE p1()
BEGIN
/** variables **/
  DECLARE nombre varchar(50);
/** cursores **/
  DECLARE cuno CURSOR FOR 
    SELECT noms FROM s;
/** condiciones **/
/** manejadores **/
/** codigo **/
    OPEN cuno; -- abrimos el cursor en el primer registro
    
    FETCH cuno INTO nombre; -- lee el noms del primer registro
                            -- y lo coloca en la variable nombre
                            -- Y coloca el cursor en el 
                            -- siguiente registro
    CLOSE cuno;
    
    SELECT nombre; -- muestro el valor de la variable

END //
DELIMITER ;

CALL p1();

DELIMITER //
DROP PROCEDURE IF EXISTS p2 //
CREATE PROCEDURE p2()
BEGIN
/** variables **/
  DECLARE nombre varchar(300);
  DECLARE resultado varchar(300);
/** cursor **/
  DECLARE cuno CURSOR FOR 
    SELECT nomp FROM p;
/** codigo **/

  OPEN cuno;
    
    FETCH cuno INTO nombre; -- mesa
    SET resultado=nombre; -- mesa

    FETCH cuno INTO nombre; -- silla
    SET resultado=CONCAT_WS(",",resultado,nombre); -- mesa,silla

    FETCH cuno INTO nombre; -- armario
    SET resultado=CONCAT_WS(",",resultado,nombre); -- mesa,silla,armario

  CLOSE cuno;
  SELECT resultado;
    
END //
DELIMITER ;

CALL p2();


-- el mismo ejemplo pero sin cursores

DELIMITER //
DROP PROCEDURE IF EXISTS p2 //
CREATE PROCEDURE p2()
BEGIN
/** variables **/
  DECLARE nombre varchar(300);
  DECLARE resultado varchar(300);
 
/** codigo **/
  SELECT nomp INTO nombre FROM p LIMIT 1; -- mesa
  SET resultado=nombre; -- mesa
  
  SELECT nomp INTO nombre FROM p LIMIT 1,1; -- silla
  set resultado=CONCAT_WS(",",resultado,nombre); -- mesa,silla

  SELECT nomp INTO nombre FROM p LIMIT 2,1; -- armario
  set resultado=CONCAT_WS(",",resultado,nombre); -- mesa,silla,armario

  SELECT resultado;
    
END //
DELIMITER ;

CALL p2();


-- quiero que retorne el nombre de todas las
-- piezas separadas por ,

DELIMITER //
DROP PROCEDURE IF EXISTS p3 //
CREATE PROCEDURE p3()
BEGIN
/** variables **/
  DECLARE nombre varchar(300);
  DECLARE resultado varchar(300);
  DECLARE ultimo boolean DEFAULT FALSE;

/** cursor **/
  DECLARE cuno CURSOR FOR 
    SELECT nomp FROM p;
/** manejadores **/
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    BEGIN 
      SET ultimo=TRUE; -- cuando lea el ultimo registro
    END;


/** codigo **/

  OPEN cuno;
    
    -- leo el primero
    FETCH cuno INTO nombre; -- mesa
    SET resultado=nombre; -- mesa
    
    /** bucle para leer los registros de la tabla uno a uno
    mientras no llegue al ultimo **/
   
    FETCH cuno INTO nombre; 
    WHILE(ultimo=FALSE) DO 
      SET resultado=CONCAT_WS(",",resultado,nombre);
      FETCH cuno INTO nombre; 
    END WHILE;
    
    CLOSE cuno;
    SELECT resultado;
    
END //
DELIMITER ;

call p3();


-- mostrar los nombres de cada pieza - color de cada pieza separados por
-- ,
-- mesa-rojo,silla-blanca,armario-gris


-- piezas separadas por ,

DELIMITER //
DROP PROCEDURE IF EXISTS p4 //
CREATE PROCEDURE p4()
BEGIN
/** variables **/
  DECLARE nombre varchar(300);
  DECLARE resultado varchar(300);
  DECLARE ultimo boolean DEFAULT FALSE;

/** cursor **/
  DECLARE cuno CURSOR FOR 
    SELECT CONCAT_WS("-",nomp,color) FROM p;
/** manejadores **/
  DECLARE CONTINUE HANDLER FOR NOT FOUND
    BEGIN 
      SET ultimo=TRUE; -- cuando lea el ultimo registro
    END;


/** codigo **/

  OPEN cuno;
    
    -- leo el primero
    FETCH cuno INTO nombre; -- mesa
    SET resultado=nombre; -- mesa
    
    /** bucle para leer los registros de la tabla uno a uno
    mientras no llegue al ultimo **/
   
    FETCH cuno INTO nombre; 
    WHILE(ultimo=FALSE) DO 
      SET resultado=CONCAT_WS(",",resultado,nombre);
      FETCH cuno INTO nombre; 
    END WHILE;
    
    CLOSE cuno;
    SELECT resultado;
    
END //
DELIMITER ;

call p4();