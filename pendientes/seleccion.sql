﻿DROP DATABASE IF EXISTS seleccion;
CREATE DATABASE seleccion;
USE seleccion;

-- procedimiento 1
-- PROCEDIMIENTO QUE RECIBE DOS NUMEROS Y ME MUESTRA 
-- SI EL PRIMER NUMERO ES MAYOR QUE EL SEGUNDO. 
-- EN CASO DE QUE SEAN IGUALES O SEA MENOR ME MUESTRA UN TEXTO VACIO.


DROP PROCEDURE IF EXISTS procedimiento1;

DELIMITER //

CREATE PROCEDURE procedimiento1(numero1 int,numero2 int)
BEGIN
  DECLARE salida varchar(30) DEFAULT "";
  
  IF (numero1>numero2) THEN 
    SET salida="numero1";
  END IF;

  SELECT salida;

END //

DELIMITER ;

-- llamamos al procedimiento
call procedimiento1(2,4);
CALL procedimiento1(4,2);


-- PROCEDIMIENTO 2
-- PROCEDIMIENTO QUE RECIBE DOS NUMEROS Y ME MUESTRA SI EL PRIMER NUMERO 
-- ES MAYOR QUE EL SEGUNDO. 
-- EN CASO DE QUE SEAN IGUALES O SEA MENOR ME MUESTRA UN TEXTO VACIO.
 


DROP PROCEDURE IF EXISTS procedimiento2;

DELIMITER //

CREATE PROCEDURE procedimiento2(numero1 int,numero2 int)
BEGIN
  DECLARE salida varchar(20) DEFAULT "";

  IF (numero1>numero2) THEN
    SET salida="numero1";
  ELSE 
    set salida="numero2";
  END IF ;

  SELECT salida;
END //

DELIMITER ;

-- llamar al procedimiento
CALL procedimiento2(2,4);
CALL procedimiento2(4,2);


-- FUNCION 1
-- FUNCION QUE RECIBE DOS NUMEROS Y ME DEVUELVE SI EL PRIMER NUMERO ES MAYOR QUE 
-- EL SEGUNDO O SI EL SEGUNDO NUMERO ES MAYOR QUE EL PRIMERO. 
-- SI LOS NUMEROS SON IGUALES ME DEVUELVE LA PALABRA IGUALES


DROP FUNCTION IF EXISTS funcion1;

DELIMITER //

CREATE FUNCTION funcion1(numero1 int,numero2 int)
RETURNS varchar(30) DETERMINISTIC
BEGIN
  DECLARE salida varchar(30) DEFAULT "";

  IF (numero1>numero2) THEN
    SET salida="numero1";
  ELSEIF (numero1=numero2) THEN 
    SET salida="iguales";
  ELSE 
    SET salida="numero2";
  END IF;
 
  RETURN salida; 


END //

DELIMITER ;

SELECT funcion1(4,2), funcion1(2,4), funcion1(2,2);


-- FUNCION 2

DROP FUNCTION IF EXISTS funcion2;

DELIMITER //

CREATE FUNCTION funcion2(nota int)
RETURNS varchar(30) DETERMINISTIC
BEGIN
  DECLARE salida varchar(30) DEFAULT "";
  IF (nota=0) THEN
    SET salida="muy deficiente";
  ELSEIF (nota>0 AND nota<5) THEN 
    SET salida="suspenso";
  ELSEIF (nota>=5 AND nota<6) THEN 
    SET salida="suficiente";
  ELSEIF (nota>=6 AND nota<7) THEN 
    SET salida="bien";
  ELSEIF (nota>=7 AND nota<9) THEN 
    SET salida="notable";
  ELSE
    SET salida="sobresaliente";
  END IF;
  
  RETURN salida; 
END //

DELIMITER ;

DROP FUNCTION IF EXISTS funcion2;

DELIMITER //

CREATE FUNCTION funcion2(nota int)
RETURNS varchar(30) DETERMINISTIC
BEGIN
  DECLARE salida varchar(30) DEFAULT "";
  IF (nota=0) THEN
    SET salida="muy deficiente";
  ELSEIF (nota<5) THEN 
    SET salida="suspenso";
  ELSEIF (nota<6) THEN 
    SET salida="suficiente";
  ELSEIF (nota<7) THEN 
    SET salida="bien";
  ELSEIF (nota<9) THEN 
    SET salida="notable";
  ELSE
    SET salida="sobresaliente";
  END IF;
  
  RETURN salida; 
END //

DELIMITER ;

SELECT funcion2(0), funcion2(3),funcion2(5),funcion2(6),funcion2(7),funcion2(10);