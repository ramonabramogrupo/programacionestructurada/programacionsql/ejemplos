﻿USE ciclistas;


-- QUIERO EL NOMBRE Y LA EDAD DE LOS CICLISTAS QUE HAN GANADO MAS DE 1 ETAPA


SELECT c.nombre,c.edad FROM ciclista c JOIN 
(SELECT e.dorsal,COUNT(*) numero FROM etapa e GROUP BY e.dorsal HAVING numero>1) c1 USING(dorsal);

SELECT c.nombre,c.edad 
FROM ciclista c 
WHERE c.dorsal IN (SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1);


-- CREAR UNA VISTA CON EL DORSAL Y EL NUMERO DE ETAPAS QUE HA GANADO
CREATE OR REPLACE VIEW ganadores AS
SELECT e.dorsal,COUNT(*) numero FROM etapa e GROUP BY e.dorsal HAVING numero>1;

-- probar la vista
SELECT * FROM ganadores;


-- CREAR OTRA VISTA para listar el nombre y la edad
CREATE or REPLACE VIEW Consulta1 AS 
SELECT c.nombre,c.edad from ciclista c JOIN ganadores g ON c.dorsal = g.dorsal; 

-- probar la vista
SELECT * FROM Consulta1 c;

CREATE OR REPLACE VIEW Consulta1A AS 
SELECT c.dorsal FROM ciclista c WHERE c.dorsal IN (SELECT g.dorsal FROM ganadores g);


-- indicame los ciclistas que han ganado mas de 1 etapa y mas de 1 puerto.

-- vista de los ganadores de puertos
CREATE OR REPLACE VIEW ganadores1 AS
SELECT p.dorsal,COUNT(*) numeroPuertos FROM puerto p GROUP BY p.dorsal HAVING COUNT(*)>1;

-- vista de los ganadores de puertos y etapas
CREATE OR REPLACE view ganadoresTotal AS 
SELECT g.dorsal FROM ganadores g JOIN ganadores1 g1 USING(dorsal)

-- terminar la consulta
-- opcion 1
SELECT * FROM ciclista c WHERE dorsal IN (SELECT * FROM ganadoresTotal t);
-- opcion 2
SELECT * FROM ciclista c JOIN ganadoresTotal t ON c.dorsal = t.dorsal;
-- opcion 3
SELECT * FROM ciclista c WHERE EXISTS (SELECT 1 FROM ganadoresTotal t WHERE t.dorsal=c.dorsal);

CREATE or REPLACE VIEW Consulta2 AS 
SELECT * FROM ciclista c WHERE EXISTS (SELECT 1 FROM ganadoresTotal t WHERE t.dorsal=c.dorsal);

SELECT * FROM Consulta2 c;


-- intentar crear una regla de validacion o check
-- edad del ciclista mayor que 18

ALTER TABLE ciclista 
ADD CHECK (edad>=18);

-- insertamos un registro para probar la regla
INSERT INTO ciclista (dorsal, nombre, edad, nomequipo)
  VALUES (1001, 'joven', 10, 'banesto');

-- si tuvieramos mysql 5.7 o anterior no soporta los checks
-- voy a crear una vista que me permita restringin los datos a introducir en la tabla

CREATE or REPLACE VIEW ciclistasJovenes AS
SELECT c.dorsal,
       c.nombre,
       c.edad,
       c.nomequipo FROM ciclista c WHERE EDAD>=18
       WITH LOCAL CHECK OPTION;

INSERT INTO ciclistasJovenes (dorsal, nombre, edad, nomequipo)
  VALUES (1002, 'JOVEN', 15, 'BANESTO');



